//Analysis functions
var AWS = require('aws-sdk'),
    Comprehend = require('aws-sdk/clients/comprehend'),
    Translate = require('aws-sdk/clients/translate'),
    async = require('async'),
    uuid = require('node-uuid');
//AWS Comprehend
var comprehend = new AWS.Comprehend({apiVersion: '2017-11-27'});
//AWS Translate
var translate = new AWS.Translate();
//IBM Watson
const ToneAnalyzerV3 = require('ibm-watson/tone-analyzer/v3');
const { IamAuthenticator } = require('ibm-watson/auth');
const toneAnalyzer = new ToneAnalyzerV3({
    authenticator: new IamAuthenticator({apikey: process.env.TONE_API_KEY}),
    version: process.env.TONE_VERSION_DATE,
    url: process.env.TONE_URL
});
//Dominant Language
var get_dominant_language = function(text_arr, callback){
    var params = {
        TextList: text_arr
    };
    //Detect
    comprehend.batchDetectDominantLanguage(params, function(err, data){
        if(err){
            callback('');
        } else {
            callback(data['ResultList']);
        }
    });
};
//Entity Recognition
var get_entity_recognition = function(text_arr, callback){
    var params = {
        LanguageCode: 'en',
        TextList: text_arr
    };
    //Detect
    comprehend.batchDetectEntities(params, function(err, data){
        if(err){
            callback('');
        } else {
            callback(data['ResultList']);
        }
    });
};
//Key Phrases
var get_key_phrases = function(text_arr, callback){
    if(text_arr.length > 25){
        var text_arr = text_arr.slice(0, 25);
    }
    var params = {
        LanguageCode: 'en',
        TextList: text_arr
    };
    //Detect
    comprehend.batchDetectKeyPhrases(params, function(err, data){
        if(err){
            callback('');
        } else {
            callback(data['ResultList']);
        }
    });
};
//Sentiment Analysis
var get_sentiment_analysis = function(text_arr, callback){
    if(text_arr.length > 25){
        var text_arr = text_arr.slice(0, 25);
    }
    var params = {
        LanguageCode: 'en',
        TextList: text_arr
    };
    //Detect
    comprehend.batchDetectSentiment(params, function(err, data){
        if(err){
            callback('');
        } else {
            callback(data['ResultList']);
        }
    });
};
//Syntax Analysis
var get_syntax_analysis = function(text_arr, callback){
    var params = {
        LanguageCode: 'en',
        TextList: text_arr
    };
    //Detect
    comprehend.batchDetectSyntax(params, function(err, data){
        if(err){
            callback('');
        } else {
            callback(data['ResultList']);
        }
    });
};
//Translate text
var get_translated_text = function(text, callback){
    var params = {
        SourceLanguageCode: 'auto',
        TargetLanguageCode: 'en',
        Text: text
    };
    //Translate
    translate.translateText(params, function(err, data){
        if(err){
            callback('');
        } else {
            callback(data);
        }
    });
};
//Tone analysis
var get_tone_analysis = function(text_arr, callback){
    if(text_arr.length > 25){
        var text_arr = text_arr.slice(0, 25);
    }
    //Results
    var results_arr = [];
    async.eachSeries(text_arr, function(text, cb){
        var params = {
            toneInput: text,
            contentType: 'text/plain',
            sentences: false
        };
        //Analyze
        toneAnalyzer.tone(params, function(err, response){
            if(err){
                cb();
            } else {
                results_arr.push(response.result.document_tone.tones);
                cb();
            }
        });
    }, function(err){
        callback(results_arr);
    });
};
//Export all functions
module.exports.get_dominant_language = get_dominant_language;
module.exports.get_entity_recognition = get_entity_recognition;
module.exports.get_key_phrases = get_key_phrases;
module.exports.get_sentiment_analysis = get_sentiment_analysis;
module.exports.get_syntax_analysis = get_syntax_analysis;
module.exports.get_translated_text = get_translated_text;
module.exports.get_tone_analysis = get_tone_analysis;
