//API - v1 - FramerSpace
var util = require('util'),
    async = require('async'),
    mongoose = require('mongoose'),
    validator = require('validator'),
    getSlug = require('speakingurl'),
    linkify = require('linkifyjs'),
    linkifyHtml = require('linkifyjs/html'),
    uuid = require('node-uuid'),
    shortid = require('shortid'),
    randomColor = require('randomcolor'),
    _ = require('lodash');
//Models
var User = require('../app/models/user').User;
var Member = require('../app/models/entity').Member,
    Course = require('../app/models/entity').Course,
    Badge = require('../app/models/entity').Badge,
    Attachment = require('../app/models/entity').Attachment,
    Response = require('../app/models/entity').Response,
    Option = require('../app/models/entity').Option,
    Fill = require('../app/models/entity').Fill,
    Item = require('../app/models/entity').Item,
    Comment = require('../app/models/entity').Comment,
    FeedbackBadge = require('../app/models/entity').FeedbackBadge,
    Feedback = require('../app/models/entity').Feedback,
    Block = require('../app/models/entity').Block,
    Note = require('../app/models/entity').Note;
//Utilities
var Utility = require('../app/utility');
//Analysis
var Analysis = require('../app/analysis');
//Email
var Email = require('../config/mail.js');
//Variables for file upload
var mime = require('mime'),
    moment = require('moment'),
    crypto = require('crypto'),
    aws = require('aws-sdk');
//Realtime functions
var online = require('../app/online');
var IO;
//Page size
var PAGE_SIZE = 20;
//Export all API functions
module.exports = function(app, passport, io){
    IO = io;
    /* ----------------- COURSE API ------------------ */
    //GET Requests
    //Get courses
    app.get('/api/courses', isLoggedIn, _getCourses);
    //Get one course details
    app.get('/api/course/:_id', isLoggedIn, _getCourseByIdOrSlug);
    //POST Requests
    //Create a course
    app.post('/api/course', isLoggedIn, _createCourse);
    //PUT Requests
    //Update a course or actions on a course
    app.put('/api/course/:_id/:_action', isLoggedIn, function(req, res){
        switch(req.params._action){
            case 'edit':
                _editCourse(req, res);
                break;
            case 'activate':
                _activateCourse(req, res);
                break;
            case 'deactivate':
                _deactivateCourse(req, res);
                break;
            case 'join':
                _joinCourse(req, res);
                break;
            case 'unjoin':
                _unjoinCourse(req, res);
                break;
            case 'add_member':
                _addMemberToCourse(req, res);
                break;
            case 'edit_member':
                _editMemberPrivilegeInCourse(req, res);
                break;
            case 'remove_member':
                _removeMemberFromCourse(req, res);
                break;
             case 'add_learner':
                _addLearnerToCourse(req, res);
                break;
            case 'remove_learner':
                _removeLearnerFromCourse(req, res);
                break;
            default:
                _editCourse(req, res);
        }
    });
    //DELETE Requests
    //Delete a course
    app.delete('/api/course/:_id', isLoggedIn, _deleteCourse);
    /* ----------------- BLOCK API ------------------------- */
    //Get Requests
    //Get course blocks
    app.get('/api/blocks/:_id', isLoggedIn, _getCourseBlocks);
    //Get container blocks
    app.get('/api/blocks/container/:_id', isLoggedIn, _getContainerBlocks);
    //Get block by _id or slug
    app.get('/api/block/:_id', isLoggedIn, _getBlockByIdOrSlug);
    //POST Requests
    //Create a block
    app.post('/api/block/:_type', isLoggedIn, function(req, res){
        switch(req.params._type){
            case 'text':
                _createTextBlock(req, res);
                break;
            case 'button':
                _createButtonBlock(req, res);
                break;
            case 'divider':
                _createDividerBlock(req, res);
                break;
            case 'toggle_list':
                _createToggleListBlock(req, res);
                break;
            case 'image':
            case 'video':
            case 'audio':
            case 'file':
                _createFileBlock(req, res);
                break;
            case 'link':
                _createLinkBlock(req, res);
                break;
            case 'gif':
                _createGIFBlock(req, res);
                break;
            case 'mcq':
                _createMCQBlock(req, res);
                break;
            case 'fill':
                _createFillInTheBlanksBlock(req, res);
                break;
            case 'match':
                _createMatchTheFollowingBlock(req, res);
                break;
            case 'response':
                _createResponseBlock(req, res);
                break;
            case 'list':
                _createListBlock(req, res);
                break;
            case 'container':
                _createContainerBlock(req, res);
                break;
            case 'grid':
                _createGridBlock(req, res);
                break;
            case 'comic':
                _createComicBlock(req, res);
                break;
            case 'embed':
                _createEmbedBlock(req, res);
                break;
            default:
                res.status(500).send({error: "Invalid query type"});
        }
    });
    //PUT Requests
    //Update a block or actions on a block
    app.put('/api/block/:_id/:_action', isLoggedIn, function(req, res){
        switch(req.params._action){
            case 'edit':
                _editBlock(req, res);
                break;
            case 'edit_text':
                _editTextBlock(req, res);
                break;
            case 'add_item':
                _addToggleListItem(req, res);
                break;
            case 'remove_item':
                _removeToggleListItem(req, res);
                break;
            case 'add_option':
                _addOption(req, res);
                break;
            case 'edit_mcq_option':
                _editMCQOption(req, res);
                break;
            case 'remove_option':
                _removeOption(req, res);
                break;
            case 'select_option':
                _selectOption(req, res);
                break;
            case 'unselect_option':
                _unselectOption(req, res);
                break;
            case 'select_match':
                _selectMatchOption(req, res);
                break;
            case 'unselect_match':
                _unselectMatchOption(req, res);
                break;
            case 'add_fill':
                _addFill(req, res);
                break;
            case 'remove_fill':
                _removeFill(req, res);
                break;
            case 'fill_blanks':
                _fillBlanks(req, res);
                break;
            case 'add_response':
                _addUserResponseToBlock(req, res);
                break;
            case 'edit_text_response':
                _editTextResponseBlock(req, res);
                break;
            case 'remove_response':
                _removeUserResponseFromBlock(req, res);
                break;
            case 'add_list_item':
                _addListItem(req, res);
                break;
            case 'remove_list_item':
                _removeListItem(req, res);
                break;
            case 'add_grid_item':
                _addGridItem(req, res);
                break;
            case 'remove_grid_item':
                _removeGridItem(req, res);
                break;
            case 'add_feedback':
                _addFeedback(req, res);
                break;
            case 'remove_feedback':
                _removeFeedback(req, res);
                break;
            default:
                _editBlock(req, res);
        }
    });
    //DELETE Requests
    //Delete block
    app.delete('/api/block/:_id', isLoggedIn, _deleteBlock);
    /* ----------------- BADGES API ------------------------- */
    //GET Requests
    //Get all badges of a course
    app.get('/api/badges/:_id', isLoggedIn, _getBadges);
    //Get a badge by id
    app.get('/api/badge/:_id', isLoggedIn, _getBadgeById);
    //POST Requests
    //Create a badge
    app.post('/api/badge', isLoggedIn, _createBadge);
    //DELETE Requests
    //Delete a badge
    app.delete('/api/badge/:_id', isLoggedIn, _deleteBadge);
    /* ----------------- COMMENTS API ------------------------- */
    //GET Requests
    //Get all comments
    app.get('/api/discussion/:_id/comments', isLoggedIn, _showComments);
    //Get a comment by id
    app.get('/api/comment/:_id', isLoggedIn, _getCommentById);
    //POST Requests
    //Add a comment
    app.post('/api/comment', isLoggedIn, _addComment);
    // PUT Requests
    // Update a comment or actions on a comment
    app.put('/api/comment/:_id/:_action', isLoggedIn, function(req, res){
        switch(req.params._action){
            case 'edit':
                _editComment(req, res);
                break;
            case 'like':
                _likeComment(req, res);
                break;
            case 'unlike':
                _unlikeComment(req, res);
                break;
            default:
                _editComment(req, res);
        }
    });
    //DELETE Requests
    //Delete a comment
    app.delete('/api/comment/:_id', isLoggedIn, _deleteComment);
    /* ----------------- USER API  ------------------ */
    //Get current user details
    app.get('/api/me', isLoggedIn, _getCurrentUser);
    //Update current user
    app.post('/api/me', isLoggedIn, _updateCurrentUser);
    //Get all users
    app.get('/api/users/:_type', isLoggedIn, function(req, res){
        switch(req.params._type){
            case 'active':
                _getAllActiveUsers(req, res);
                break;
            case 'inactive':
                _getAllInactiveUsers(req, res);
                break;
            default:
                _getAllActiveUsers(req, res);
        }
    });
    //Get public user details
    app.get('/api/user/:_id', isLoggedIn, _getPublicUser);
    //PUT Requests
    //Update a user
    app.put('/api/user/:_id/:_action', isLoggedIn, function(req, res){
        switch(req.params._action){
            case 'activate':
                _activateUser(req, res);
                break;
            case 'deactivate':
                _deactivateUser(req, res);
                break;
            case 'unique_id':
                _updateUniqueId(req, res);
                break;
            default:
                _activateUser(req, res);
        }
    });
    /* ----------------- SEARCH API  ------------------ */
    //Get search results
    app.get('/api/search/:_type', isLoggedIn, function(req, res){
        switch(req.params._type){
            case 'users':
                _searchUsers(req, res);
                break;
            case 'gifs':
                _searchGifs(req, res);
                break;
            case 'badges':
                _searchBadges(req, res);
                break;
            case 'skills':
                _searchSkills(req, res);
                break;
            default:
                _searchUsers(req, res);
        }
    });
    /* ----------------- INSIGHT API  ------------------ */
    app.get('/api/insight/:_id/:_type', isLoggedIn, function(req, res){
        switch(req.params._type){
            case 'basic':
                _getCourseBasicInsight(req, res);
                break;
            case 'users':
                _getCourseUsersInsight(req, res);
                break;
            case 'user':
                _getCourseUserInsight(req, res);
                break;
            default:
                res.status(500).send({error: "Invalid query type"});
        }
    });
    /* ----------------- GET LINK DETAILS ------------------ */
    app.get('/api/embedlink', isLoggedIn, _getLinkPreview);
    /* ----------------- GET UNIQUE NAME ------------------ */
    app.get('/api/uniquename', isLoggedIn, _getUniqueName);
    /* ----------------- ANALYSIS ------------------ */
    app.post('/api/analysis/:_type', isLoggedIn, function(req, res){
        switch(req.params._type){
            case 'language':
                _getDominantLanguage(req, res);
                break;
            case 'entity':
                _getEntityRecognition(req, res);
                break;
            case 'keyphrase':
                _getKeyPhrases(req, res);
                break;
            case 'sentiment':
                _getSentimentAnalysis(req, res);
                break;
            case 'syntax':
                _getSyntaxAnalysis(req, res);
                break;
            case 'translate':
                _getTranslatedText(req, res);
                break;
            case 'tone':
                _getToneAnalysis(req, res);
                break;
            default:
                res.status(500).send({error: "Invalid query type"});
        }
    });
    /* ----------------- UPLOAD TO S3 ------------------ */
    app.get('/api/signed', isLoggedIn, _uploadS3);
};
/*---------------- COURSE FUNCTION -------------------------*/
//GET Request functions - Course
var _getCourses = function(req, res){
    Course.find({
        is_active: true,
        $or: [{privacy: 'public'},
              {creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: {$in: ['moderator', 'teacher', 'active']}}}}]
    }).select('-members -learners').populate('creator', 'name initials username dp job', 'User')
    .sort({updated_at: -1}).exec(function(err, courses){
        res.send(courses);
    });
};
//Get one course details
var _getCourseByIdOrSlug = function(req, res){
    //Match if object id or not
    if(req.params._id.match(/^[0-9a-fA-F]{24}$/)){
        var query = {
            _id: req.params._id
        }
    } else {
        var query = {
            slug: req.params._id
        }
    }
    Course.findOne(query).populate('creator', 'name initials username dp job', 'User')
    .populate('members.user', 'name initials username dp job', 'User')
    .exec(function(err, course){
        if(!course) return res.sendStatus(404);
        res.send(course);
    });
};
//POST Requests function - Course
//Create a course
var _createCourse  = function(req, res){
    if(!req.body.title){
        return res.status(400).send({error: "Invalid parameters. We are expecting a title"});
    }
    //Slug
    var key = shortid.generate();
    var slug = key + '-' + getSlug(req.body.title);
    //New course
    var new_course = new Course({
        slug: slug,
        title: req.body.title,
        tagline: req.body.tagline,
        'image.m': req.body.image,
        'image.l': req.body.image,
        bound: req.body.bound,
        'tag.core': req.body.core,
        'tag.sel': req.body.sel,
        'tag.sdg': req.body.sdg,
        privacy: req.body.privacy,
        creator: req.user.id,
        updated_at: new Date(Date.now())
    });
    //Color
    new_course.color.a = randomColor();
    //Join code if private
    if(req.body.privacy == 'private'){
        new_course.join_code = shortid.generate();
    }
    //Save
    new_course.save(function(err){
        if(!err) res.send(new_course);
        //Update image
        if(req.body.image){
            var image = req.body.image.replace(/^https:\/\//i, 'http://');
            var m_file_name = 'm-' + slug;
            //Update image (medium size)
            Utility.get_resized_image(m_file_name, image, 800, function(resized){
                Utility.upload_file(resized, m_file_name, function(image_url){
                    Course.update({ _id: new_course._id }, { $set: { 'image.m': image_url }}).exec();
                });
            });
        }
    });
};
//PUT Requests functions - Course
//Edit basic details of course like title, tagline etc.
var _editCourse = function(req, res){
    Course.findOne({
        _id: req.params._id,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }, function(err, course){
        if(!course) return res.status(400).send({error: "Unauthorized user. Cannot edit course."});
        //Update title
        if(req.body.title){
            course.title = req.body.title;
        }
        //Update tagline
        if(req.body.tagline != null){
            course.tagline = req.body.tagline;
        }
        //Update image
        if(req.body.image){
            //Get previous image keys
            if(course.image) var keys = Utility.get_image_keys([course.image.l], course.image.m);
            //Update
            course.image.m = req.body.image;
            course.image.l = req.body.image;
            course.bound = req.body.bound;
        }
        //Update organisation
        if(req.body.org_name){
            course.org.name = req.body.org_name;
        }
        if(req.body.org_logo){
            course.org.logo = req.body.org_logo;
        }
        if(req.body.org_url){
            course.org.url = req.body.org_url;
        }
        //Update privacy
        if(req.body.privacy){
            course.privacy = req.body.privacy;
            //If course is private and no join code
            if(!course.join_code && course.privacy == 'private'){
                course.join_code = shortid.generate();
            }
        }
        //Update tag
        if(req.body.core){
            course.tag.core = req.body.core;
        }
        if(req.body.sel){
            course.tag.sel = req.body.sel;
        }
        if(req.body.sdg){
            course.tag.sdg = req.body.sdg;
        }
        //Update theme
        if(req.body.color_a){
            course.color.a = req.body.color_a;
        }
        if(req.body.color_a){
            course.color.b = req.body.color_a;
        }
        course.updated_at = new Date(Date.now());
        //Save
        course.save(function(err){
            if(!err) {
                res.status(200).send(course);
                //Update image
                if(req.body.image){
                    var key = shortid.generate();
                    var slug = key + '-' + getSlug(course.title);
                    var image = req.body.image.replace(/^https:\/\//i, 'http://');
                    var m_file_name = 'm-' + slug;
                    //Update image (medium size)
                    Utility.get_resized_image(m_file_name, image, 800, function(resized){
                        Utility.upload_file(resized, m_file_name, function(image_url){
                            Course.update({ _id: course._id }, { $set: { 'image.m': image_url }}).exec();
                        });
                    });
                    //Delete previous images
                    if(keys) Utility.delete_keys(keys);
                }
            } else {
                res.sendStatus(400);
            }
        });
    });
};
//Activate course - only by creator
var _activateCourse = function(req, res){
    Course.update({ _id: req.params._id, creator: req.user.id}, { $set : { is_active: true } }, function(err, numAffected){
        res.sendStatus(200);
    });
};
//Deactivate course - only by creator
var _deactivateCourse = function(req, res){
    Course.update({ _id: req.params._id, creator: req.user.id}, { $set : { is_active: false } }, function(err, numAffected){
        res.sendStatus(200);
    });
};
//Join course
var _joinCourse = function(req, res){
    Course.findOne({_id: req.params._id}, function(err, course){
        if(!course) return res.status(400).send({error: "No such course exists"});
        //Get all course members
        var member_ids = [];
        for(var i=0; i<course.members.length; i++){
            if(course.members[i].user)
                member_ids.push(course.members[i].user.toString());
        }
        if(member_ids.indexOf(req.user.id.toString()) > -1) {
            return res.status(400).send({error: "Already joined"});
        } else if (req.user.id.toString() == course.creator) {
            return res.status(400).send({error: "Cannot add creator to members list"});
        } else {
            //Add new member in inactive state
            var new_member = new Member({
                user: req.user.id,
                added_at: new Date(Date.now()),
                permit_val: 'inactive'
            });
            course.members.push(new_member);
            course.save(function(err){
                if(!err) res.sendStatus(200);
            });
        }
    });
};
//Unjoin course
var _unjoinCourse = function(req, res){
    Course.update({_id: req.params._id}, {$pull: {members: {user: mongoose.Types.ObjectId(req.user.id), permit_val: 'inactive'}}}, function(err, numberAffected){
        if(!err) res.sendStatus(200);
        else return res.sendStatus(400);
    });
};
//Add member to course
var _addMemberToCourse = function(req, res){
    if(!req.body.email && !req.body.user_id){
        //Expecting a email id or user_id
        return res.status(400).send({error: "Invalid parameters. We are expecting a user_id or user email."});
    } else if ((req.body.email == req.user.email) || (req.body.user_id == req.user.id)) {
       //Cannot collaborate to current user
       return res.status(400).send({error: "Cannot add yourself."});
   }
   //Check for user
    var user_id, user_email;
    async.series([
        function(callback){
            if(req.body.user_id){
                user_id = req.body.user_id;
                callback();
            } else if(req.body.email){
                User.findOne({email: req.body.email}, function(err, user){
                    if(!user){
                        user_email = req.body.email;
                        callback();
                    } else {
                        user_id = user._id;
                        callback();
                    }
                });
            }
        }
    ], function(err){
        //Find course
        Course.findOne({
            _id: req.params._id,
            $or: [{creator: req.user.id},
                  {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
        }, function(err, course){
            if(!course) return res.status(400).send({error: "No such course exists or Unauthorized user."});
            if(user_id){
                //Remove member from inactive state if present
                Course.update({_id: course._id},{$pull: {members: {user: mongoose.Types.ObjectId(user_id), permit_val: 'inactive'}}}, function(err, numberAffected){
                    //Get all course members
                    var member_ids = [];
                    for(var i=0; i<course.members.length; i++){
                        if(course.members[i].user && !(course.members[i].user == user_id && course.members[i].permit_val == 'inactive')){
                            member_ids.push(course.members[i].user.toString());
                        }
                    }
                    if(member_ids.indexOf(user_id.toString()) > -1) {
                        return res.status(400).send({error: "Already added."});
                    } else if (user_id.toString() == course.creator) {
                        return res.status(400).send({error: "Cannot add creator to collaborator list"});
                    } else {
                        var new_member = new Member({
                            user: user_id,
                            added_by: req.user.id,
                            added_at: new Date(Date.now()),
                            permit_val: 'active'
                        });
                        course.members.push(new_member);
                        course.count.members += 1;
                        course.save(function(err){
                            if(!err) {
                                new_member.populate({path: 'user', select: 'name initials username dp job'}, function(err, member){
                                    res.send(member);
                                    //Send email
                                    User.findOne({_id: user_id}, function(err, user){
                                        if(!user.email) return;
                                        var content = {
                                            email: user.email,
                                            name: user.name,
                                            firstName: user.name.split(' ')[0],
                                            fromName: req.user.name,
                                            subject: req.user.name.split(' ')[0] + " has added you to a course on FramerSpace.",
                                            title: course.title,
                                            redirectURL: course.slug
                                        };
                                        Email.sendOneMail('course_invite', content, function(err, responseStatus){});
                                    });
                                });
                            }
                        });
                    }
                });
            } else if(user_email){
                var member_ids = [];
                for(var i=0; i<course.members.length; i++){
                    if(course.members[i].email)
                        member_ids.push(course.members[i].email);
                }
                if(member_ids.indexOf(user_email) > -1) {
                    return res.status(400).send({error: "Already invited."});
                } else {
                    //Save member
                    var new_member = new Member({ permit_val: 'invited', email: user_email, added_by: req.user.id });
                    course.members.push(new_member);
                    course.save(function(err){
                        res.send(new_member);
                        //Send email
                        var content = {
                            email: user_email,
                            fromName: req.user.name,
                            subject: req.user.name.split(' ')[0] + " has invited you to join a course on FramerSpace.",
                            title: course.title,
                            redirectURL: course.slug
                        };
                        Email.sendOneMail('course_invite_new', content, function(err, responseStatus){});
                    });
                }
            }
        });
    });
};
//Edit member privilege in course
var _editMemberPrivilegeInCourse = function(req, res){
    if(!req.body.user_id){
        return res.status(400).send({error: "Invalid parameters. We are expecting a user_id."});
    } else if (!req.body.permit_val) {
       return res.status(400).send({error: "Invalid parameters. We are expecting a permit_val."});
   } else if((req.body.permit_val != 'active') && (req.body.permit_val != 'moderator') && (req.body.permit_val != 'teacher')){
       return res.status(400).send({error: "Invalid parameters. Incorrect permit_val."});
   }
   //Update
   Course.update({
       _id: req.params._id,
       $or: [{ creator: req.user.id },
             { members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}],
       'members.user': req.body.user_id
   }, { $set: { 'members.$.permit_val': req.body.permit_val} }, function(err, numAffected){
       if(!err) res.sendStatus(200);
   });
};
//Remove member from course
var _removeMemberFromCourse = function(req, res){
    if(req.body.email){
        //Remove invited user
        Course.update({
            _id: req.params._id,
            $or: [{ creator: req.user.id },
                  { members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]},
            {$pull: {members: {email: req.body.email}}}, function(err, numAffected){
                if(!err) res.sendStatus(200);
                else return res.sendStatus(400);
        });
    } else if(req.body.user_id){
        //Remove user
        Course.update({
            _id: req.params._id,
            $or: [{ creator: req.user.id },
                  { members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]},
            {$pull: {members: {user: mongoose.Types.ObjectId(req.body.user_id)}, $inc: {'count.members': -1}}}, function(err, numAffected){
                if(!err) {
                    res.sendStatus(200);
                }
                else return res.sendStatus(400);
        });
    } else {
        //Leave
        Course.update({_id: req.params._id},
            {$pull: {members: {user: mongoose.Types.ObjectId(req.user.id)}, $inc: {'count.members': -1}}}, function(err, numAffected){
                if(!err) {
                    res.sendStatus(200);
                }
                else return res.sendStatus(400);
        });
    }
};
//Add learner to course
var _addLearnerToCourse = function(req, res){

};
//Remove learner from course
var _removeLearnerFromCourse = function(req, res){

};
//DELETE Request function
//Delete course
var _deleteCourse = function(req, res){

};
/*---------------- BLOCK FUNCTION -------------------------*/
//GET Request functions - Block
//Get blocks of a course
var _getCourseBlocks = function(req, res){
    Course.findOne({
        _id: req.params._id,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}},
              {is_active: true, privacy: {$in: ['public', 'unlisted']}},
              {is_active: true, members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: {$in: ['teacher', 'active']}}}}]
    }, function(err, course){
        if(!course) return res.send([]);
        //Show hidden blocks if creator, moderator or teacher
        Course.findOne({
            _id: req.params._id,
            $or: [{creator: req.user.id},
                  {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: {$in: ['moderator', 'teacher']}}}}]
        }, function(err, creator_course){
            if(!creator_course){
                Block.find({course: course._id, is_hidden: false, container: null})
                .select({ order: 1, slug: 1, type: 1, course: 1, is_active: 1, is_hidden: 1, title: 1, summary: 1, text: 1, button: 1, divider: 1, image: 1, bound: 1, file: 1, provider: 1, embed: 1, gif: 1, mcqs: 1, is_multiple: 1, fills: 1, options: 1, response_type:1, responses: { $elemMatch: { creator: req.user.id }}, items: 1, rows: 1, cols: 1, theme: 1, art: 1, size: 1, feedbacks: 1, alt_text: 1, ref_url: 1, extra: 1, has_discussion: 1, comments: { $elemMatch: { is_recent: true }}, creator: 1, created_at: 1, updated_at: 1})
                .populate('creator', 'name initials username dp', 'User')
                .populate('comments.creator', 'name initials username dp job', 'User')
                .sort({order: 1}).exec(function(err, blocks){
                    res.send(blocks);
                });
            } else {
                Block.find({course: course._id, container: null})
                .select({ order: 1, slug: 1, type: 1, course: 1, is_active: 1, is_hidden: 1, title: 1, summary: 1, text: 1, button: 1, divider: 1, image: 1, bound: 1, file: 1, provider: 1, embed: 1, gif: 1, mcqs: 1, is_multiple: 1, fills: 1, options: 1, response_type:1, responses: { $elemMatch: { creator: req.user.id }}, items: 1, rows: 1, cols: 1, theme: 1, art: 1, size: 1, feedbacks: 1, alt_text: 1, ref_url: 1, extra: 1, has_discussion: 1, comments: { $elemMatch: { is_recent: true }}, creator: 1, created_at: 1, updated_at: 1})
                .populate('creator', 'name initials username dp', 'User')
                .populate('comments.creator', 'name initials username dp job', 'User')
                .sort({order: 1}).exec(function(err, blocks){
                    res.send(blocks);
                });
            }
        });
    });
};
//Get blocks of a container
var _getContainerBlocks = function(req, res){
    Block.findOne({_id: req.params._id, type: 'container'}, function(err, container){
        if(!container) return res.send([]);
        Course.findOne({
            _id: container.course,
            $or: [{creator: req.user.id},
                  {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}},
                  {is_active: true, privacy: {$in: ['public', 'unlisted']}},
                  {is_active: true, members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: {$in: ['teacher', 'active']}}}}]
        }, function(err, course){
            if(!course) return res.send([]);
            //Show hidden blocks if creator, moderator or teacher
            Course.findOne({
                _id: container.course,
                $or: [{creator: req.user.id},
                      {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: {$in: ['moderator', 'teacher']}}}}]
            }, function(err, creator_course){
                if(!creator_course){
                    Block.find({course: course._id, is_hidden: false, container: container._id})
                    .select({ order: 1, slug: 1, type: 1, course: 1, is_active: 1, is_hidden: 1, title: 1, summary: 1, text: 1, button: 1, divider: 1, image: 1, bound: 1, file: 1, provider: 1, embed: 1, gif: 1, mcqs: 1, is_multiple: 1, fills: 1, options: 1, response_type:1, responses: { $elemMatch: { creator: req.user.id }}, items: 1, rows: 1, cols: 1, theme: 1, art: 1, size: 1, feedbacks: 1, alt_text: 1, ref_url: 1, extra: 1, has_discussion: 1, comments: { $elemMatch: { is_recent: true }}, creator: 1, created_at: 1, updated_at: 1})
                    .populate('creator', 'name initials username dp', 'User')
                    .populate('comments.creator', 'name initials username dp job', 'User')
                    .sort({order: 1}).exec(function(err, blocks){
                        res.send(blocks);
                    });
                } else {
                    Block.find({course: course._id, container: container._id})
                    .select({ order: 1, slug: 1, type: 1, course: 1, is_active: 1, is_hidden: 1, title: 1, summary: 1, text: 1, button: 1, divider: 1, image: 1, bound: 1, file: 1, provider: 1, embed: 1, gif: 1, mcqs: 1, is_multiple: 1, fills: 1, options: 1, response_type:1, responses: { $elemMatch: { creator: req.user.id }}, items: 1, rows: 1, cols: 1, theme: 1, art: 1, size: 1, feedbacks: 1, alt_text: 1, ref_url: 1, extra: 1, has_discussion: 1, comments: { $elemMatch: { is_recent: true }}, creator: 1, created_at: 1, updated_at: 1})
                    .populate('creator', 'name initials username dp', 'User')
                    .populate('comments.creator', 'name initials username dp job', 'User')
                    .sort({order: 1}).exec(function(err, blocks){
                        res.send(blocks);
                    });
                }
            });
        });
    });
};
//Get one block
var _getBlockByIdOrSlug = function(req, res){
    //Match if object id or not
    if(req.params._id.match(/^[0-9a-fA-F]{24}$/)){
        var query = {
            _id: req.params._id
        };
    } else {
        var query = {
            slug: req.params._id
        };
    }
    //Find
    Block.findOne(query).populate('creator', 'name initials username dp job', 'User')
    .populate('mcqs.voters', 'name initials username dp job', 'User')
    .populate('fills.responses.creator', 'name initials username dp job', 'User')
    .populate('options.matchers.creator', 'name initials username dp job', 'User')
    .populate('feedbacks.badges.badge', 'title color image bound is_skill skill_total', 'Badge')
    .populate('responses.creator', 'name initials username dp job', 'User')
    .populate('comments.creator', 'name initials username dp job', 'User').exec(function(err, block){
        if(!block) return res.sendStatus(404);
        //Check if user has access to course
        Course.findOne({
            _id: block.course,
            $or: [{creator: req.user.id},
                  {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}},
                  {is_active: true, privacy: {$in: ['public', 'unlisted']}},
                  {is_active: true, members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: {$in: ['teacher', 'active']}}}}]
        }, function(err, course){
            if(!course) return res.status(400).send({error: "Unauthorized user. Cannot view block."});
            res.send(block);
        });
    });
};
//POST Request functions - Block
//Create text block
var _createTextBlock = function(req, res){
    if(!req.body.course){
        return res.status(400).send({error: "Invalid parameters. We are expecting a course id"});
    }
    if(!req.body.title && !req.body.text){
        return res.status(400).send({error: "Invalid parameters. We are expecting a title or a text."});
    }
    //Check access to course
    Course.findOne({
        _id: req.body.course,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }).exec(function(err, course){
        if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add block to this course."});
        var slug = shortid.generate();
        //Create new block
        var new_block = new Block({
            slug: slug,
            order: req.body.order,
            type: 'text',
            course: req.body.course,
            container: req.body.container,
            title: req.body.title,
            text: req.body.text,
            'image.m': req.body.image,
            'image.l': req.body.image,
            bound: req.body.bound,
            images: req.body.images,
            creator: req.user.id,
            updated_at: new Date(Date.now())
        });
        //Save block
        new_block.save(function(err){
            //Update order of other blocks
            if(req.body.order){
                Block.update({_id: {$ne: new_block._id}, course: course._id, order: {$gte: req.body.order}}, {$inc: {order: 1}}, {multi: true}, function(err, numAffected){
                    res.send(new_block);
                });
            } else {
                res.send(new_block);
            }
            //Update image
            if(req.body.image){
                var image = req.body.image.replace(/^https:\/\//i, 'http://');
                var m_file_name = 'm-' + slug;
                //Update image (medium size)
                Utility.get_resized_image(m_file_name, image, 400, function(resized){
                    Utility.upload_file(resized, m_file_name, function(image_url){
                        Block.update({ _id: new_block._id }, { $set: { 'image.m': image_url }}).exec();
                    });
                });
            }
        });
    });
};
//Create button block
var _createButtonBlock = function(req, res){
    if(!req.body.course){
        return res.status(400).send({error: "Invalid parameters. We are expecting a course id"});
    }
    if(!req.body.text){
        return res.status(400).send({error: "Invalid parameters. We are expecting a button text."});
    }
    //Check access to course
    Course.findOne({
        _id: req.body.course,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }).exec(function(err, course){
        if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add block to this course."});
        var slug = shortid.generate();
        //Create new block
        var new_block = new Block({
            slug: slug,
            order: req.body.order,
            type: 'button',
            course: req.body.course,
            container: req.body.container,
            text: req.body.text,
            'button.url': req.body.button_url,
            'button.block': req.body.button_block,
            'button.is_new_tab': req.body.is_new_tab,
            creator: req.user.id,
            updated_at: new Date(Date.now())
        });
        //Save block
        new_block.save(function(err){
            //Update order of other blocks
            if(req.body.order){
                Block.update({_id: {$ne: new_block._id}, course: course._id, order: {$gte: req.body.order}}, {$inc: {order: 1}}, {multi: true}, function(err, numAffected){
                    res.send(new_block);
                });
            } else {
                res.send(new_block);
            }
        });
    });
};
//Create button block
var _createDividerBlock = function(req, res){
    if(!req.body.course){
        return res.status(400).send({error: "Invalid parameters. We are expecting a course id"});
    }
    //Check access to course
    Course.findOne({
        _id: req.body.course,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }).exec(function(err, course){
        if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add block to this course."});
        var slug = shortid.generate();
        //Create new block
        var new_block = new Block({
            slug: slug,
            order: req.body.order,
            type: 'divider',
            course: req.body.course,
            container: req.body.container,
            text: req.body.text,
            'divider.type': req.body.divider_type,
            'divider.time': req.body.divider_time,
            'divider.name': req.body.divider_name,
            creator: req.user.id,
            updated_at: new Date(Date.now())
        });
        //Save block
        new_block.save(function(err){
            //Update order of other blocks
            if(req.body.order){
                Block.update({_id: {$ne: new_block._id}, course: course._id, order: {$gte: req.body.order}}, {$inc: {order: 1}}, {multi: true}, function(err, numAffected){
                    res.send(new_block);
                });
            } else {
                res.send(new_block);
            }
        });
    });
};
//Create toggle list block
var _createToggleListBlock = function(req, res){
    if(!req.body.course){
        return res.status(400).send({error: "Invalid parameters. We are expecting a course id"});
    }
    if(!req.body.title || !req.body.text){
        return res.status(400).send({error: "Invalid parameters. We are expecting a title and a text"});
    }
    //Check access to course
    Course.findOne({
        _id: req.body.course,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }).exec(function(err, course){
        if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add block to this course."});
        var slug = shortid.generate();
        //Create new block
        var new_block = new Block({
            slug: slug,
            order: req.body.order,
            type: 'toggle_list',
            course: req.body.course,
            container: req.body.container,
            creator: req.user.id,
            updated_at: new Date(Date.now())
        });
        //Add item
        var new_item = new Item({
            title: req.body.title,
            creator: req.user.id,
            updated_at: new Date(Date.now())
        });
        //Linkify text
        var linkifiedText = linkifyHtml(req.body.text, {
            target: '_blank'
        });
        new_item.text = linkifiedText.replace(/\n\r?/g, '<br />');
        //Push
        new_block.items.push(new_item);
        //Save block
        new_block.save(function(err){
            //Update order of other blocks
            if(req.body.order){
                Block.update({_id: {$ne: new_block._id}, course: course._id, order: {$gte: req.body.order}}, {$inc: {order: 1}}, {multi: true}, function(err, numAffected){
                    res.send(new_block);
                });
            } else {
                res.send(new_block);
            }
        });
    });
};
//Create image, video, audio or file block
var _createFileBlock = function(req, res){
    if(!req.body.course){
        return res.status(400).send({error: "Invalid parameters. We are expecting a course id"});
    }
    if(!req.body.provider.url)
        return res.status(400).send({error: "Invalid parameters. We are expecting an url."});
    //Check access to course
    Course.findOne({
        _id: req.body.course,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }).exec(function(err, course){
        if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add block to this course."});
        var slug = shortid.generate();
        //Create new block
        var new_block = new Block({
            slug: slug,
            order: req.body.order,
            type: req.params._type,
            course: req.body.course,
            container: req.body.container,
            title: req.body.title,
            text: req.body.text,
            provider: req.body.provider,
            'image.m': req.body.image,
            'image.l': req.body.image,
            bound: req.body.bound,
            file: req.body.file,
            creator: req.user.id,
            updated_at: new Date(Date.now())
        });
        //Save block
        new_block.save(function(err){
            //Update order of other blocks
            if(req.body.order){
                Block.update({_id: {$ne: new_block._id}, course: course._id, order: {$gte: req.body.order}}, {$inc: {order: 1}}, {multi: true}, function(err, numAffected){
                    res.send(new_block);
                });
            } else {
                res.send(new_block);
            }
            //Resize image
            if(req.body.image){
                var file_name = slug;
                if(req.body.provider.name == 'FramerSpace'){
                    var image = req.body.image.replace(/^https:\/\//i, 'http://');
                    //Resize and upload image
                    Utility.get_resized_image(file_name, image, 400, function(resized){
                        Utility.upload_file(resized, file_name, function(image_url){
                            Block.update({ _id: new_block._id }, { $set: { 'image.m': image_url }}).exec();
                        });
                    });
                } else {
                    //Download and upload image
                    Utility.download_file(req.body.image, file_name, function(file){
                        Utility.upload_file(file, file_name, function(image_url){
                            Block.update({ _id: new_block._id }, { $set: { 'image.m': image_url }}).exec();
                        });
                    });
                }
            }
        });
    });
};
//Create link block
var _createLinkBlock = function(req, res){
    if(!req.body.course){
        return res.status(400).send({error: "Invalid parameters. We are expecting a course id"});
    }
    if((!req.body.url || !validator.isURL(req.body.url)) && !req.body.linkdata){
        return res.status(400).send({error: "Invalid parameters. We are expecting a valid url or link data"});
    }
    //Check access to course
    Course.findOne({
        _id: req.body.course,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }).exec(function(err, course){
        if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add block to this course."});
        var linkdata;
        async.series([
            //Get link metadata
            function(callback){
                if(req.body.linkdata){
                    linkdata = req.body.linkdata;
                    callback();
                } else {
                    Utility.get_link_metadata(req.body.url, function(data){
                        linkdata = data;
                        //Get image
                        var images = data.images;
                        var imageURL;
                        if(images && images.length){
                            for(var i=0; i<images.length; i++){
                                if(images[i].width > 200 && images[i].height > 100){
                                    req.body.image = images[i].url.replace(/^https:\/\//i, 'http://');
                                    //Set bound
                                    var bound = (images[i].height * 400 ) / images[i].width;
                                    if(bound){
                                        bound = parseInt(bound);
                                        req.body.bound = bound;
                                    }
                                    break;
                                }
                            }
                        }
                        callback();
                    });
                }
            }
        ], function(err){
            //Slug
            var slug = shortid.generate();
            //Create new block
            var new_block = new Block({
                slug: slug,
                order: req.body.order,
                type: 'link',
                course: req.body.course,
                container: req.body.container,
                title: linkdata.title || linkdata.url,
                text: linkdata.description || req.body.summary,
                'provider.name': linkdata.provider_name,
                'provider.url': linkdata.url,
                'provider.favicon': linkdata.favicon_url,
                'embed.code': linkdata.media.html,
                'embed.kind': linkdata.media.type || linkdata.type,
                publish_date: linkdata.published,
                'image.m': req.body.image,
                'image.l': req.body.image,
                bound: req.body.bound,
                creator: req.user.id,
                updated_at: new Date(Date.now())
            });
            //Save block
            new_block.save(function(err){
                //Update order of other blocks
                if(req.body.order){
                    Block.update({_id: {$ne: new_block._id}, course: course._id, order: {$gte: req.body.order}}, {$inc: {order: 1}}, {multi: true}, function(err, numAffected){
                        res.send(new_block);
                    });
                } else {
                    res.send(new_block);
                }
                //Update image
                if(req.body.image){
                    var image = req.body.image.replace(/^https:\/\//i, 'http://');
                    var file_name = slug;
                    var m_file_name = 'm-' + file_name;
                    //Download and update original file
                    Utility.download_file(image, file_name, function(file){
                        Utility.upload_file(file, file_name, function(image_url){
                            Block.update({ _id: new_block._id }, { $set: { 'image.l': image_url }}).exec();
                        });
                    });
                    //Update image (medium size)
                    Utility.get_resized_image(m_file_name, image, 400, function(resized){
                        Utility.upload_file(resized, m_file_name, function(image_url){
                            Block.update({ _id: new_block._id }, { $set: { 'image.m': image_url }}).exec();
                        });
                    });
                }
            });
        });
    });
};
//Create gif block
var _createGIFBlock = function(req, res){
    if(!req.body.course){
        return res.status(400).send({error: "Invalid parameters. We are expecting a course id"});
    }
    if(!req.body.gif_embed){
        return res.status(400).send({error: "Invalid parameters. We are expecting a gif_embed."});
    }
    //Check access to course
    Course.findOne({
        _id: req.body.course,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }).exec(function(err, course){
        if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add block to this course."});
        var slug = shortid.generate();
        //Create new block
        var new_block = new Block({
            slug: slug,
            order: req.body.order,
            type: 'gif',
            course: req.body.course,
            container: req.body.container,
            'gif.embed': req.body.gif_embed,
            'gif.url': req.body.gif_url,
            'gif.width': req.body.width,
            'gif.height': req.body.height,
            creator: req.user.id,
            updated_at: new Date(Date.now())
        });
        //Save block
        new_block.save(function(err){
            //Update order of other blocks
            if(req.body.order){
                Block.update({_id: {$ne: new_block._id}, course: course._id, order: {$gte: req.body.order}}, {$inc: {order: 1}}, {multi: true}, function(err, numAffected){
                    res.send(new_block);
                });
            } else {
                res.send(new_block);
            }
        });
    });
};
//Create MCQ and Image MCQ block
var _createMCQBlock = function(req, res){
    if(!req.body.course){
        return res.status(400).send({error: "Invalid parameters. We are expecting a course id"});
    }
    if(!req.body.title && !req.body.text){
        return res.status(400).send({error: "Invalid parameters. We are expecting a title or a text."});
    }
    //Check access to course
    Course.findOne({
        _id: req.body.course,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }).exec(function(err, course){
        if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add block to this course."});
        var slug = shortid.generate();
        //Create new block
        var new_block = new Block({
            slug: slug,
            order: req.body.order,
            type: 'mcq',
            course: req.body.course,
            container: req.body.container,
            title: req.body.title,
            is_multiple: req.body.is_multiple,
            creator: req.user.id,
            updated_at: new Date(Date.now())
        });
        //Block text
        if(req.body.text){
            var linkifiedText = linkifyHtml(req.body.text, {
                target: '_blank'
            });
            new_block.text = linkifiedText.replace(/\n\r?/g, '<br />');
        }
        //Save block
        new_block.save(function(err){
            //Update order of other blocks
            if(req.body.order){
                Block.update({_id: {$ne: new_block._id}, course: course._id, order: {$gte: req.body.order}}, {$inc: {order: 1}}, {multi: true}, function(err, numAffected){
                    res.send(new_block);
                });
            } else {
                res.send(new_block);
            }
        });
    });
};
//Create fill in the blanks block
var _createFillInTheBlanksBlock = function(req, res){
    if(!req.body.course){
        return res.status(400).send({error: "Invalid parameters. We are expecting a course id"});
    }
    if(!req.body.title && !req.body.text){
        return res.status(400).send({error: "Invalid parameters. We are expecting a title or a text."});
    }
    //Check access to course
    Course.findOne({
        _id: req.body.course,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }).exec(function(err, course){
        if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add block to this course."});
        var slug = shortid.generate();
        //Create new block
        var new_block = new Block({
            slug: slug,
            order: req.body.order,
            type: 'fill',
            course: req.body.course,
            container: req.body.container,
            title: req.body.title,
            creator: req.user.id,
            updated_at: new Date(Date.now())
        });
        //Block text
        if(req.body.text){
            var linkifiedText = linkifyHtml(req.body.text, {
                target: '_blank'
            });
            new_block.text = linkifiedText.replace(/\n\r?/g, '<br />');
        }
        //Save block
        new_block.save(function(err){
            //Update order of other blocks
            if(req.body.order){
                Block.update({_id: {$ne: new_block._id}, course: course._id, order: {$gte: req.body.order}}, {$inc: {order: 1}}, {multi: true}, function(err, numAffected){
                    res.send(new_block);
                });
            } else {
                res.send(new_block);
            }
        });
    });
};
//Create match the following block
var _createMatchTheFollowingBlock = function(req, res){
    if(!req.body.course){
        return res.status(400).send({error: "Invalid parameters. We are expecting a course id"});
    }
    if(!req.body.title && !req.body.text){
        return res.status(400).send({error: "Invalid parameters. We are expecting a title or a text."});
    }
    //Check access to course
    Course.findOne({
        _id: req.body.course,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }).exec(function(err, course){
        if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add block to this course."});
        var slug = shortid.generate();
        //Create new block
        var new_block = new Block({
            slug: slug,
            order: req.body.order,
            type: 'match',
            course: req.body.course,
            container: req.body.container,
            title: req.body.title,
            is_draggable: req.body.is_draggable,
            creator: req.user.id,
            updated_at: new Date(Date.now())
        });
        //Block text
        if(req.body.text){
            var linkifiedText = linkifyHtml(req.body.text, {
                target: '_blank'
            });
            new_block.text = linkifiedText.replace(/\n\r?/g, '<br />');
        }
        //Save block
        new_block.save(function(err){
            //Update order of other blocks
            if(req.body.order){
                Block.update({_id: {$ne: new_block._id}, course: course._id, order: {$gte: req.body.order}}, {$inc: {order: 1}}, {multi: true}, function(err, numAffected){
                    res.send(new_block);
                });
            } else {
                res.send(new_block);
            }
        });
    });
};
//Create response block
var _createResponseBlock = function(req, res){
    if(!req.body.course){
        return res.status(400).send({error: "Invalid parameters. We are expecting a course id"});
    }
    if(!req.body.title && !req.body.text){
        return res.status(400).send({error: "Invalid parameters. We are expecting a title or a text."});
    }
    if(!req.body.response_type){
        return res.status(400).send({error: "Invalid parameters. We are expecting a response type."});
    }
    //Check access to course
    Course.findOne({
        _id: req.body.course,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }).exec(function(err, course){
        if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add block to this course."});
        var slug = shortid.generate();
        //Create new block
        var new_block = new Block({
            slug: slug,
            order: req.body.order,
            type: 'response',
            course: req.body.course,
            container: req.body.container,
            title: req.body.title,
            response_type: req.body.response_type,
            creator: req.user.id,
            updated_at: new Date(Date.now())
        });
        //Block text
        if(req.body.text){
            var linkifiedText = linkifyHtml(req.body.text, {
                target: '_blank'
            });
            new_block.text = linkifiedText.replace(/\n\r?/g, '<br />');
        }
        //Save block
        new_block.save(function(err){
            //Update order of other blocks
            if(req.body.order){
                Block.update({_id: {$ne: new_block._id}, course: course._id, order: {$gte: req.body.order}}, {$inc: {order: 1}}, {multi: true}, function(err, numAffected){
                    res.send(new_block);
                });
            } else {
                res.send(new_block);
            }
        });
    });
};
//Create list block
var _createListBlock = function(req, res){
    if(!req.body.course){
        return res.status(400).send({error: "Invalid parameters. We are expecting a course id"});
    }
    if(!req.body.title && !req.body.text){
        return res.status(400).send({error: "Invalid parameters. We are expecting a title or a text."});
    }
    //Check access to course
    Course.findOne({
        _id: req.body.course,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }).exec(function(err, course){
        if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add block to this course."});
        var slug = shortid.generate();
        //Create new block
        var new_block = new Block({
            slug: slug,
            order: req.body.order,
            type: 'list',
            course: req.body.course,
            container: req.body.container,
            title: req.body.title,
            creator: req.user.id,
            updated_at: new Date(Date.now())
        });
        //Block text
        if(req.body.text){
            var linkifiedText = linkifyHtml(req.body.text, {
                target: '_blank'
            });
            new_block.text = linkifiedText.replace(/\n\r?/g, '<br />');
        }
        //Save block
        new_block.save(function(err){
            //Update order of other blocks
            if(req.body.order){
                Block.update({_id: {$ne: new_block._id}, course: course._id, order: {$gte: req.body.order}}, {$inc: {order: 1}}, {multi: true}, function(err, numAffected){
                    res.send(new_block);
                });
            } else {
                res.send(new_block);
            }
        });
    });
};
//Create container block
var _createContainerBlock = function(req, res){
    if(!req.body.course){
        return res.status(400).send({error: "Invalid parameters. We are expecting a course id"});
    }
    if(!req.body.title && !req.body.text){
        return res.status(400).send({error: "Invalid parameters. We are expecting a title or a text."});
    }
    //Check access to course
    Course.findOne({
        _id: req.body.course,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }).exec(function(err, course){
        if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add block to this course."});
        var slug = shortid.generate();
        //Create new block
        var new_block = new Block({
            slug: slug,
            order: req.body.order,
            type: 'container',
            course: req.body.course,
            container: req.body.container,
            title: req.body.title,
            'image.m': req.body.image,
            'image.l': req.body.image,
            bound: req.body.bound,
            creator: req.user.id,
            updated_at: new Date(Date.now())
        });
        //Block text
        if(req.body.text){
            var linkifiedText = linkifyHtml(req.body.text, {
                target: '_blank'
            });
            new_block.text = linkifiedText.replace(/\n\r?/g, '<br />');
        }
        //Save block
        new_block.save(function(err){
            //Update order of other blocks
            if(req.body.order){
                Block.update({_id: {$ne: new_block._id}, course: course._id, order: {$gte: req.body.order}}, {$inc: {order: 1}}, {multi: true}, function(err, numAffected){
                    res.send(new_block);
                    //Update image
                    if(req.body.image){
                        var image = req.body.image.replace(/^https:\/\//i, 'http://');
                        var m_file_name = 'm-' + slug;
                        //Update image (medium size)
                        Utility.get_resized_image(m_file_name, image, 800, function(resized){
                            Utility.upload_file(resized, m_file_name, function(image_url){
                                Block.update({ _id: new_block._id }, { $set: { 'image.m': image_url }}).exec();
                            });
                        });
                    }
                });
            } else {
                res.send(new_block);
            }
        });
    });
};
//Create grid block
var _createGridBlock = function(req, res){
    if(!req.body.course){
        return res.status(400).send({error: "Invalid parameters. We are expecting a course id"});
    }
    if(!req.body.title && !req.body.text){
        return res.status(400).send({error: "Invalid parameters. We are expecting a title or a text."});
    }
    //Check access to course
    Course.findOne({
        _id: req.body.course,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }).exec(function(err, course){
        if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add block to this course."});
        var slug = shortid.generate();
        //Create new block
        var new_block = new Block({
            slug: slug,
            order: req.body.order,
            type: 'grid',
            course: req.body.course,
            container: req.body.container,
            title: req.body.title,
            creator: req.user.id,
            updated_at: new Date(Date.now())
        });
        //Block text
        if(req.body.text){
            var linkifiedText = linkifyHtml(req.body.text, {
                target: '_blank'
            });
            new_block.text = linkifiedText.replace(/\n\r?/g, '<br />');
        }
        //Save block
        new_block.save(function(err){
            //Update order of other blocks
            if(req.body.order){
                Block.update({_id: {$ne: new_block._id}, course: course._id, order: {$gte: req.body.order}}, {$inc: {order: 1}}, {multi: true}, function(err, numAffected){
                    res.send(new_block);
                });
            } else {
                res.send(new_block);
            }
        });
    });
};
//Create comic block
var _createComicBlock = function(req, res){
    if(!req.body.course){
        return res.status(400).send({error: "Invalid parameters. We are expecting a course id"});
    }
    if(!req.body.text && !req.body.image){
         return res.status(400).send({error: "Invalid parameters. We are expecting a comic text or a comic image."});
    }
    //Check access to course
    Course.findOne({
        _id: req.body.course,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }).exec(function(err, course){
        if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add block to this course."});
        var slug = shortid.generate();
        //Create new block
        var new_block = new Block({
            slug: slug,
            order: req.body.order,
            type: 'comic',
            course: req.body.course,
            container: req.body.container,
            text: req.body.text,
            'image.m': req.body.image,
            'image.l': req.body.image,
            creator: req.user.id,
            updated_at: new Date(Date.now())
        });
        //Save block
        new_block.save(function(err){
            //Update order of other blocks
            if(req.body.order){
                Block.update({_id: {$ne: new_block._id}, course: course._id, order: {$gte: req.body.order}}, {$inc: {order: 1}}, {multi: true}, function(err, numAffected){
                    res.send(new_block);
                });
            } else {
                res.send(new_block);
            }
        });
    });
};
//Create embed block
var _createEmbedBlock = function(req, res){
    if(!req.body.course){
        return res.status(400).send({error: "Invalid parameters. We are expecting a course id"});
    }
    if(!req.body.embed_code){
        return res.status(400).send({error: "Invalid parameters. We are expecting an embed_code"});
    }
    //Check access to course
    Course.findOne({
        _id: req.body.course,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }).exec(function(err, course){
        if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add block to this course."});
        var slug = shortid.generate();
        //Create new block
        var new_block = new Block({
            slug: slug,
            order: req.body.order,
            type: 'embed',
            course: req.body.course,
            container: req.body.container,
            title: req.body.title,
            'embed.code': req.body.embed_code,
            'embed.width': req.body.width,
            'embed.height': req.body.height,
            creator: req.user.id,
            updated_at: new Date(Date.now())
        });
        //Save block
        new_block.save(function(err){
            //Update order of other blocks
            if(req.body.order){
                Block.update({_id: {$ne: new_block._id}, course: course._id, order: {$gte: req.body.order}}, {$inc: {order: 1}}, {multi: true}, function(err, numAffected){
                    res.send(new_block);
                });
            } else {
                res.send(new_block);
            }
        });
    });
};
//PUT Request functions - Block
//Edit basic details of a block like title, button etc.
var _editBlock = function(req, res){
    Block.findOne({_id: req.params._id}, function(err, block){
        if(!block) return res.sendStatus(404);
        //Check access to course
        Course.findOne({
            _id: block.course,
            $or: [{creator: req.user.id},
                  {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
        }).exec(function(err, course){
            if(!course) return res.status(400).send({error: "Unauthorized user. Cannot edit block."});
            //Update title
            if(req.body.title){
                block.title = req.body.title;
            }
            //Update text
            if(req.body.text){
                var linkifiedText = linkifyHtml(req.body.text, {
                    target: '_blank'
                });
                block.text = linkifiedText.replace(/\n\r?/g, '<br />');
            }
            //Update button
            if(req.body.button_url != null){
                block.button.url = req.body.button_url;
            }
            if(req.body.button_block != null){
                block.button.block = req.body.button_block;
            }
            if(req.body.is_new_tab != null){
                block.button.is_new_tab = req.body.is_new_tab;
            }
            //Update divider
            if(req.body.divider_time != null){
                block.divider.time = req.body.divider_time;
            }
            if(req.body.divider_type != null){
                block.divider.type = req.body.divider_type;
            }
            if(req.body.divider_name != null){
                block.divider.name = req.body.divider_name;
            }
            //Is multiple
            if(req.body.is_multiple != null){
                block.is_multiple = req.body.is_multiple;
            }
            //Update embed block
            if(req.body.embed_code){
                block.embed.code = req.body.embed_code;
            }
            if(req.body.embed_width != null){
                block.embed.width = req.body.embed_width;
            }
            if(req.body.embed_height != null){
                block.embed.height = req.body.embed_height;
            }
            //Update theme
            if(req.body.theme != null){
                block.theme = req.body.theme;
            }
            //Update art
            if(req.body.art != null){
                //Get previous image keys
                if(block.art) var keys = Utility.get_image_keys([block.art.l], block.art.m);
                //Update
                block.art.m = req.body.art;
                block.art.l = req.body.art;
                block.art.bound = req.body.art_bound;
            }
            //Update container image
            if(block.type == 'container' && req.body.image != null){
                //Get previous image keys
                if(block.image) var keys = Utility.get_image_keys([block.image.l], block.image.m);
                //Update
                block.image.m = req.body.image;
                block.image.l = req.body.image;
                block.bound = req.body.bound;
            }
            //Update size
            if(req.body.width){
                block.size.width = req.body.width;
            }
            if(req.body.margin != null){
                block.size.margin = req.body.margin;
            }
            //Publish discussion
            if(req.body.has_discussion != null){
                block.has_discussion = req.body.has_discussion;
            }
            if(req.body.is_restricted != null){
                block.is_restricted = req.body.is_restricted;
            }
            //Is hidden
            if(req.body.is_hidden != null){
                block.is_hidden = req.body.is_hidden;
            }
            block.updated_at = new Date(Date.now());
            //Save
            block.save(function(err){
                if(!err){
                    res.status(200).send(block);
                    //Update art
                    if(req.body.art){
                        var key = shortid.generate();
                        var slug = key + '-' + getSlug(block.title);
                        var image = req.body.art.replace(/^https:\/\//i, 'http://');
                        var m_file_name = 'm-' + slug;
                        //Update image (medium size)
                        Utility.get_resized_image(m_file_name, image, 400, function(resized){
                            Utility.upload_file(resized, m_file_name, function(image_url){
                                Block.update({ _id: block._id }, { $set: { 'art.m': image_url }}).exec();
                            });
                        });
                        //Delete previous images
                        if(keys) Utility.delete_keys(keys);
                    }
                    //Update image of container
                    if(block.type == 'container' && req.body.image){
                        var key = shortid.generate();
                        var slug = key + '-' + getSlug(block.title);
                        var image = req.body.image.replace(/^https:\/\//i, 'http://');
                        var m_file_name = 'm-' + slug;
                        //Update image (medium size)
                        Utility.get_resized_image(m_file_name, image, 800, function(resized){
                            Utility.upload_file(resized, m_file_name, function(image_url){
                                Block.update({ _id: block._id }, { $set: { 'image.m': image_url }}).exec();
                            });
                        });
                        //Delete previous images
                        if(keys) Utility.delete_keys(keys);
                    }
                } else res.sendStatus(400);
            });
        });
    });
};
//Edit text block
var _editTextBlock = function(req, res){
    if(!req.body.text){
        return res.status(400).send({error: "Invalid parameters. We are expecting a block text."});
    }
    Block.findOne({_id: req.params._id, type: 'text'}, function(err, block){
        if(!block) return res.sendStatus(404);
        //Check access to course
        Course.findOne({
            _id: block.course,
            $or: [{creator: req.user.id},
                  {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
        }).exec(function(err, course){
            if(!course) return res.status(400).send({error: "Unauthorized user. Cannot edit block."});
            block.text = req.body.text;
            //Update images
            if(req.body.images){
                block.images = _.union(block.images, req.body.images);
            }
            //Update image
            if(req.body.image && (!block.image || (block.image && block.image.l != req.body.image))){
                //Add previous thumbnail in images
                if(block.image.m) block.images.push(block.image.m);
                //Update image
                block.image.l = req.body.image;
                block.image.m = req.body.image;
                if(req.body.bound) block.bound = req.body.bound;
            } else {
                req.body.image = '';
            }
            block.updated_at = new Date(Date.now());
            //Save
            block.save(function(err){
                if(!err){
                    res.status(200).send(block);
                    //Update image
                    if(req.body.image){
                        var image = req.body.image.replace(/^https:\/\//i, 'http://');
                        var m_file_name = 'm-' + block.slug;
                        //Update image (medium size)
                        Utility.get_resized_image(m_file_name, image, 400, function(resized){
                            Utility.upload_file(resized, m_file_name, function(image_url){
                                Block.update({ _id: block._id }, { $set: { 'image.m': image_url }}).exec();
                            });
                        });
                    }
                }
            });
        });
    });
};
//Add toggle list item
var _addToggleListItem = function(req, res){
    if(!req.body.title || !req.body.text){
        return res.status(400).send({error: "Invalid parameters. We are expecting a title and a text"});
    }
    Block.findOne({_id: req.params._id}, function(err, block){
        if(!block) return res.sendStatus(404);
        //Check access to course
        Course.findOne({
            _id: block.course,
            $or: [{creator: req.user.id},
                  {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
        }).exec(function(err, course){
            if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add item."});
            //Add new item
            var new_item = new Item({
                title: req.body.title,
                creator: req.user.id,
                updated_at: new Date(Date.now())
            });
            //Linkify text
            var linkifiedText = linkifyHtml(req.body.text, {
                target: '_blank'
            });
            new_item.text = linkifiedText.replace(/\n\r?/g, '<br />');
            //Push
            block.items.push(new_item);
            //Save
            block.save(function(err){
                if(!err) res.send(new_item);
            });
        });
    });
};
//Remove toggle list item
var _removeToggleListItem = function(req, res){
    if(!req.body.item){
        return res.status(400).send({error: "Invalid parameters. We are expecting an item id."});
    }
    Block.findOne({_id: req.params._id}, function(err, block){
        if(!block) return res.sendStatus(404);
        //Check access to course
        Course.findOne({
            _id: block.course,
            $or: [{creator: req.user.id},
                  {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
        }).exec(function(err, course){
            if(!course) return res.status(400).send({error: "Unauthorized user. Cannot remove item."});
            Block.update({
                _id: req.params._id,
                'items._id': req.body.item
            }, { $pull: { items: {_id: req.body.item}}}, function(err, numAffected){
                res.sendStatus(200);
            });
        });
    });
};
//Add option for MCQ or Match the following
var _addOption = function(req, res){
    if(!req.body.text){
        return res.status(400).send({error: "Invalid parameters. We are expecting an option text."});
    }
    Block.findOne({_id: req.params._id}, function(err, block){
        if(!block) return res.sendStatus(404);
        //Check access to course
        Course.findOne({
            _id: block.course,
            $or: [{creator: req.user.id},
                  {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
        }).exec(function(err, course){
            if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add option."});
            //Add new option
            var new_option = new Option({
                text: req.body.text,
                'image.m': req.body.image,
                'image.l': req.body.image,
                bound: req.body.bound
            });
            //Check if mcq or match the following
            if(block.type == 'mcq'){
                block.mcqs.push(new_option);
            } else if(block.type == 'match'){
                new_option.is_optionb = req.body.is_optionb;
                block.options.push(new_option);
            }
            //Save
            block.save(function(err){
                if(!err){
                    res.status(200).send(new_option);
                    //Update image
                    if(req.body.image){
                        var image = req.body.image.replace(/^https:\/\//i, 'http://');
                        var slug = shortid.generate();
                        var m_file_name = 'm-' + slug;
                        //Update image (medium size)
                        Utility.get_resized_image(m_file_name, image, 200, function(resized){
                            Utility.upload_file(resized, m_file_name, function(image_url){
                                if(block.type == 'mcq'){
                                    Block.update({_id: req.params._id, 'mcqs._id': new_option._id}, {$set: {'mcqs.$.image.m': image_url}}).exec();
                                } else if(block.type == 'match'){
                                    Block.update({_id: req.params._id, 'options._id': new_option._id}, {$set: {'options.$.image.m': image_url}}).exec();
                                }
                            });
                        });
                    }
                } else res.sendStatus(400);
            });
        });
    });
};
//Edit MCQ option
var _editMCQOption = function(req, res){
    if(!req.body.option){
        return res.status(400).send({error: "Invalid parameters. We are expecting an option id."});
    }
    Block.findOne({_id: req.params._id}, function(err, block){
        if(!block) return res.sendStatus(404);
        //Check access to course
        Course.findOne({
            _id: block.course,
            $or: [{creator: req.user.id},
                  {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
        }).exec(function(err, course){
            if(!course) return res.status(400).send({error: "Unauthorized user. Cannot edit option."});
            Block.update({
                _id: req.params._id,
                'mcqs._id': req.body.option
            }, { $set: { 'mcqs.$.text': req.body.text} }, function(err, numAffected){
                if(!err) res.send({text: req.body.text});
            });
        });
    });
};
//Remove option from MCQ or Match the following
var _removeOption = function(req, res){
    if(!req.body.option){
        return res.status(400).send({error: "Invalid parameters. We are expecting an option id."});
    }
    Block.findOne({_id: req.params._id}, function(err, block){
        if(!block) return res.sendStatus(404);
        //Check access to course
        Course.findOne({
            _id: block.course,
            $or: [{creator: req.user.id},
                  {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
        }).exec(function(err, course){
            if(!course) return res.status(400).send({error: "Unauthorized user. Cannot remove option."});
            //Check if mcq or match the following
            if(block.type == 'mcq'){
                Block.update({
                    _id: req.params._id,
                    'mcqs._id': req.body.option
                }, { $pull: { mcqs: {_id: req.body.option}}}, function(err, numAffected){
                    res.sendStatus(200);
                });
            } else if(block.type == 'match'){
                Block.update({
                    _id: req.params._id,
                    'options._id': req.body.option
                }, { $pull: { options: {_id: req.body.option}}}, function(err, numAffected){
                    res.sendStatus(200);
                });
            }
        });
    });
};
//Select MCQ Option
var _selectOption = function(req, res){
    if(!req.body.option){
        return res.status(400).send({error: "Invalid parameters. We are expecting an option id."});
    }
    Block.findOne({_id: req.params._id}, function(err, block){
        if(!block) return res.sendStatus(404);
        //Check access to course
        Course.findOne({
            _id: block.course,
            $or: [{creator: req.user.id},
                  {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}},
                  {is_active: true, privacy: {$in: ['public', 'unlisted']}},
                  {is_active: true, members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: {$in: ['teacher', 'active']}}}}]
        }).exec(function(err, course){
            if(!course) return res.status(400).send({error: "Unauthorized user. Cannot select option."});
            if(block.is_multiple){
                Block.update({
                    _id: req.params._id,
                    'mcqs._id': req.body.option
                }, { $addToSet: { "mcqs.$.voters": req.user.id }}, function(err, numAffected){
                    res.sendStatus(200);
                });
            } else {
                //Remove all previous votes
                Block.update({
                    _id: req.params._id,
                    "mcqs.voters": req.user.id
                }, { $pull: { "mcqs.$.voters": req.user.id } }, function(err, numAffected) {
                    //Add new vote
                    Block.update({
                        _id: req.params._id,
                        'mcqs._id': req.body.option
                    }, { $addToSet: { "mcqs.$.voters": req.user.id }}, function(err, numAffected){
                        res.sendStatus(200);
                    });
                });
            }
        });
    });
};
//Unselect option
var _unselectOption = function(req, res){
    if(!req.body.option){
        return res.status(400).send({error: "Invalid parameters. We are expecting an option id."});
    }
    Block.update({
        _id: req.params._id,
        "mcqs._id": req.body.option
    }, { $pull: { "mcqs.$.voters": req.user.id } }, function(err, numAffected) {
        if(!err) res.sendStatus(200);
    });
};
//Select match option
var _selectMatchOption = function(req, res){
    if(!req.body.option){
        return res.status(400).send({error: "Invalid parameters. We are expecting an option id."});
    }
    Block.findOne({_id: req.params._id}, function(err, block){
        if(!block) return res.sendStatus(404);
        //Check access to course
        Course.findOne({
            _id: block.course,
            $or: [{creator: req.user.id},
                  {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}},
                  {is_active: true, privacy: {$in: ['public', 'unlisted']}},
                  {is_active: true, members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: {$in: ['teacher', 'active']}}}}]
        }).exec(function(err, course){
            if(!course) return res.status(400).send({error: "Unauthorized user. Cannot select option."});
            if(req.body.matched_to){
                //New matcher
                var new_matcher = new Response({
                    matched_to: req.body.matched_to,
                    creator: req.user.id,
                    updated_at: new Date(Date.now())
                });
                //Check if already matched
                var selected_option;
                for(var i=0; i<block.options.length; i++){
                    if(block.options[i]._id.toString() == req.body.option){
                        selected_option = block.options[i];
                        break;
                    }
                }
                if(selected_option){
                    for(var i=0; i<selected_option.matchers.length; i++){
                        if(selected_option.matchers[i].creator.toString() == req.user.id && selected_option.matchers[i].matched_to.toString() == req.body.matched_to){
                            return res.status(400).send({error: "Already matched."});
                        }
                    }
                }
                //Match
                Block.update({
                    _id: req.params._id,
                    "options._id": req.body.option},
                { $push: { "options.$.matchers": new_matcher } }, function(err, numAffected){
                    res.send(new_matcher);
                });
            } else {
                //Get random color
                var optionColor = randomColor({luminosity: 'dark'});
                //Update color of option
                Block.update({
                    _id: req.params._id,
                    "options._id": req.body.option
                }, { $set: { "options.$.color": optionColor } }, function(err, numAffected) {
                    if(!err) res.send({color: optionColor});
                });
            }
        });
    });
};
//Unselect match option
var _unselectMatchOption = function(req, res){
    if(!req.body.option || !req.body.matched_to){
        return res.status(400).send({error: "Invalid parameters. We are expecting an option id and matched_to option id."});
    }
    Block.update({
        _id: req.params._id,
        "options._id": req.body.option
    }, { $pull: { "options.$.matchers": {matched_to: req.body.matched_to, creator: req.user.id} } }, function(err, numAffected) {
        if(!err) res.sendStatus(200);
    });
};
//Add fill
var _addFill = function(req, res){
    Block.findOne({_id: req.params._id}, function(err, block){
        if(!block) return res.sendStatus(404);
        //Check access to course
        Course.findOne({
            _id: block.course,
            $or: [{creator: req.user.id},
                  {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
        }).exec(function(err, course){
            if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add fill."});
            //Add new fill
            var new_fill = new Fill({
                text: req.body.text,
                is_blank: req.body.is_blank,
                size: req.body.size,
                options: req.body.options
            });
            block.fills.push(new_fill);
            //Save
            block.save(function(err){
                if(!err){
                    res.status(200).send(new_fill);
                } else res.sendStatus(400);
            });
        });
    });
};
//Remove fill
var _removeFill = function(req, res){
    if(!req.body.fill){
        return res.status(400).send({error: "Invalid parameters. We are expecting an fill id."});
    }
    Block.findOne({_id: req.params._id}, function(err, block){
        if(!block) return res.sendStatus(404);
        //Check access to course
        Course.findOne({
            _id: block.course,
            $or: [{creator: req.user.id},
                  {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
        }).exec(function(err, course){
            if(!course) return res.status(400).send({error: "Unauthorized user. Cannot remove fill."});
            Block.update({
                _id: req.params._id,
                'fills._id': req.body.fill
            }, { $pull: { fills: {_id: req.body.fill}}}, function(err, numAffected){
                res.sendStatus(200);
            });
        });
    });
};
//Fill blanks
var _fillBlanks = function(req, res){
    if(!req.body.fills || !req.body.fills.length){
        return res.status(400).send({error: "Invalid parameters. We are expecting a fills array."});
    }
    Block.findOne({_id: req.params._id}, function(err, block){
        if(!block) return res.sendStatus(404);
        //Check access to course
        Course.findOne({
            _id: block.course,
            $or: [{creator: req.user.id},
                  {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}},
                  {is_active: true, privacy: {$in: ['public', 'unlisted']}},
                  {is_active: true, members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: {$in: ['teacher', 'active']}}}}]
        }).exec(function(err, course){
            if(!course) return res.status(400).send({error: "Unauthorized user. Cannot fill blanks."});
            //Fill
            async.each(req.body.fills, function(fill, callback){
                var fill_id = fill[0];
                var fill_text =  fill[1];
                if(fill_text){
                    var new_fill = new Response({
                        text: fill_text,
                        creator: req.user.id,
                        updated_at: new Date(Date.now())
                    });
                    //Remove previous fill
                    Block.update({
                        _id: req.params._id,
                        "fills._id": fill_id
                    }, { $pull: { "fills.$.responses": {creator: req.user.id} } }, function(err, numAffected) {
                        //Fill
                        Block.update({
                            _id: req.params._id,
                            "fills._id": fill_id},
                        { $push: { "fills.$.responses": new_fill}}, function(err, numAffected){
                            callback();
                        });
                    });
                } else {
                    Block.update({
                        _id: req.params._id,
                        "fills._id": fill_id
                    }, { $pull: { "fills.$.responses": {creator: req.user.id} } }, function(err, numAffected) {
                        callback();
                    });
                }
            }, function(err){
                res.sendStatus(200);
            });
        });
    });
};
//Add user response
var _addUserResponseToBlock = function(req, res){
    if(!req.body.text && !req.body.provider){
         return res.status(400).send({error: "Invalid parameters. We are expecting a response text or a response url."});
    }
    Block.findOne({_id: req.params._id}, function(err, block){
        if(!block) return res.sendStatus(404);
            //Check access to course
            Course.findOne({
                _id: block.course,
                $or: [{creator: req.user.id},
                      {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}},
                      {is_active: true, privacy: {$in: ['public', 'unlisted']}},
                      {is_active: true, members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: {$in: ['teacher', 'active']}}}}]
            }, function(err, course){
                if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add response."});
                //Add new response
                var new_response = new Response({
                    text: req.body.text,
                    creator: req.user.id,
                    updated_at: new Date(Date.now)
                });
                //Add attachment
                if(req.body.provider && req.body.provider.url){
                    var new_attachment = new Attachment({
                        type: req.body.file_type || block.response_type,
                        file: req.body.file,
                        provider: req.body.provider
                    });
                    new_response.attachments.push(new_attachment);
                }
                //Save
                block.responses.push(new_response);
                block.save(function(err){
                    if(!err){
                        res.status(200).send(block);
                    } else res.sendStatus(400);
                });
            });
    });
};
//Edit response
var _editTextResponseBlock = function(req, res){
    if(!req.body.text){
        return res.status(400).send({error: "Invalid parameters. We were expecting a response text."});
    }
    Block.update({
        _id: req.params._id,
        response_type: 'text',
        'responses.creator': req.user.id
    }, { $set: { 'responses.$.text': req.body.text} }, function(err, numAffected){
        if(!err) res.send({text: req.body.text});
    });
};
//Remove user response
var _removeUserResponseFromBlock = function(req, res){
    Block.findOne({_id: req.params._id}, function(err, block){
        if(!block) return res.sendStatus(404);
        if(!block.responses || !block.responses.length)
            return res.status(400).send({error: "Cannot remove response."});
        //Find provider url if any
        var keys = [];
        for(var i=0; i<block.responses.length; i++){
            if((block.responses[i].creator == req.user.id) && block.responses[i].attachments.length){
                for(var j=0; j<block.responses[i].attachments.length; j++){
                    var provider_key = Utility.get_provider_key(block.responses[i].attachments[j].provider);
                    keys.push(provider_key);
                }
                break;
            }
        }
        //Update block
        Block.update({
            _id: req.params._id,
            'responses.creator': req.user.id
        }, { $pull: { responses: {creator: req.user.id}}}, function(err, numAffected){
            if(!err){
                res.sendStatus(200);
                //Finally delete all keys
                Utility.delete_keys(keys);
            } else {
                res.sendStatus(400);
            }
        });
    });
};
//Add list item
var _addListItem = function(req, res){
    if(!req.body.text && !req.body.image){
        return res.status(400).send({error: "Invalid parameters. We are expecting an item text or image."});
    }
    Block.findOne({_id: req.params._id}, function(err, block){
        if(!block) return res.sendStatus(404);
        //Check access to course
        Course.findOne({
            _id: block.course,
            $or: [{creator: req.user.id},
                  {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
        }).exec(function(err, course){
            if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add item."});
            //Add new item
            var new_item = new Item({
                type: req.body.item_type,
                text: req.body.text,
                'image.m': req.body.image,
                'image.l': req.body.image,
                bound: req.body.bound,
                is_right: req.body.is_right
            });
            block.items.push(new_item);
            //Save
            block.save(function(err){
                if(!err){
                    res.status(200).send(new_item);
                    //Update image
                    if(req.body.image){
                        var image = req.body.image.replace(/^https:\/\//i, 'http://');
                        var slug = shortid.generate();
                        var m_file_name = 'm-' + slug;
                        //Update image (medium size)
                        Utility.get_resized_image(m_file_name, image, 200, function(resized){
                            Utility.upload_file(resized, m_file_name, function(image_url){
                                Block.update({_id: req.params._id, 'items._id': new_item._id}, {$set: {'items.$.image.m': image_url}}).exec();
                            });
                        });
                    }
                } else res.sendStatus(400);
            });
        });
    });
};
//Remove list item
var _removeListItem = function(req, res){
    if(!req.body.item){
        return res.status(400).send({error: "Invalid parameters. We are expecting an item id."});
    }
    Block.findOne({_id: req.params._id}, function(err, block){
        if(!block) return res.sendStatus(404);
        //Check access to course
        Course.findOne({
            _id: block.course,
            $or: [{creator: req.user.id},
                  {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
        }).exec(function(err, course){
            if(!course) return res.status(400).send({error: "Unauthorized user. Cannot remove item."});
            Block.update({
                _id: req.params._id,
                'items._id': req.body.item
            }, { $pull: { items: {_id: req.body.item}}}, function(err, numAffected){
                res.sendStatus(200);
            });
        });
    });
};
//Add grid item
var _addGridItem = function(req, res){
    if(!req.body.text && !req.body.image){
        return res.status(400).send({error: "Invalid parameters. We are expecting an item text or image."});
    }
    Block.findOne({_id: req.params._id}, function(err, block){
        if(!block) return res.sendStatus(404);
        //Check access to course
        Course.findOne({
            _id: block.course,
            $or: [{creator: req.user.id},
                  {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
        }).exec(function(err, course){
            if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add item."});
            //Add new item
            var new_item = new Item({
                text: req.body.text,
                'image.m': req.body.image,
                'image.l': req.body.image,
                bound: req.body.bound
            });
            block.items.push(new_item);
            //Save
            block.save(function(err){
                if(!err){
                    res.status(200).send(new_item);
                    //Update image
                    if(req.body.image){
                        var image = req.body.image.replace(/^https:\/\//i, 'http://');
                        var slug = shortid.generate();
                        var m_file_name = 'm-' + slug;
                        //Update image (medium size)
                        Utility.get_resized_image(m_file_name, image, 400, function(resized){
                            Utility.upload_file(resized, m_file_name, function(image_url){
                                Block.update({_id: req.params._id, 'items._id': new_item._id}, {$set: {'items.$.image.m': image_url}}).exec();
                            });
                        });
                    }
                } else res.sendStatus(400);
            });
        });
    });
};
//Remove grid item
var _removeGridItem = function(req, res){
    if(!req.body.item){
        return res.status(400).send({error: "Invalid parameters. We are expecting an item id."});
    }
    Block.findOne({_id: req.params._id}, function(err, block){
        if(!block) return res.sendStatus(404);
        //Check access to course
        Course.findOne({
            _id: block.course,
            $or: [{creator: req.user.id},
                  {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
        }).exec(function(err, course){
            if(!course) return res.status(400).send({error: "Unauthorized user. Cannot remove item."});
            Block.update({
                _id: req.params._id,
                'items._id': req.body.item
            }, { $pull: { items: {_id: req.body.item}}}, function(err, numAffected){
                res.sendStatus(200);
            });
        });
    });
};
//Add feedback
var _addFeedback = function(req, res){
    Block.findOne({_id: req.params._id}, function(err, block){
        if(!block) return res.sendStatus(404);
        //Check access to course
        Course.findOne({
            _id: block.course,
            $or: [{creator: req.user.id},
                  {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
        }).exec(function(err, course){
            if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add feedback."});
            //Add new feedback
            var new_feedback = new Feedback({
                text: req.body.text,
                badges: req.body.badges,
                selected_options: req.body.selected_options,
                fill_id: req.body.fill_id,
                fill_items: req.body.fill_items
            });
            block.feedbacks.push(new_feedback);
            //Save
            block.save(function(err){
                if(!err) {
                    res.status(200).send(new_feedback);
                    //Update total count of skill
                    if(req.body.badges && req.body.badges.length){
                        for(var i=0; i<req.body.badges.length; i++){
                            if(req.body.badges[i].skill_inc){
                                var skill_inc = parseInt(req.body.badges[i].skill_inc);
                                Badge.update({ _id: req.body.badges[i].badge, is_skill: true }, { $inc: { skill_total:  skill_inc}}).exec();
                            }
                        }
                    }
                }
                else res.sendStatus(400);
            })
        });
    });
};
//Remove feedback
var _removeFeedback = function(req, res){

};
//DELETE Request functions - Block
//Delete block
var _deleteBlock = function(req, res){
    Block.findOne({_id: req.params._id}, function(err, block){
        if(!block) return res.sendStatus(404);
        //Check access to course
        Course.findOne({
            _id: block.course,
            $or: [{creator: req.user.id},
                  {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
        }).exec(function(err, course){
            if(!course) return res.status(400).send({error: "Unauthorized user. Cannot delete block."});
            //All s3 image keys
            var keys = [];
            async.parallel([
                function(callback){
                   //Delete provider files
                   if(block.image){
                       var provider_key = Utility.get_provider_key(block.provider, block.image.m);
                   } else {
                       var provider_key = Utility.get_provider_key(block.provider);
                   }
                   if(provider_key) keys.push(provider_key);
                   callback();
                },
                function(callback){
                   //Delete images
                   if(block.type == 'text' && block.images){
                       if(block.image) {
                           var image_keys = Utility.get_image_keys(block.images, block.image.m);
                       } else {
                           var image_keys = Utility.get_image_keys(block.images);
                       }
                   } else if(block.image){
                       var image_keys = Utility.get_image_keys([block.image.l], block.image.m);
                   }
                   keys = keys.concat(image_keys);
                   callback();
                },
                function(callback){
                    //Delete images provider files of responses
                    if(block.responses && block.responses.length){
                        for(var i=0; i<block.responses.length; i++){
                            //Images
                            if(block.responses[i].images && block.responses[i].images.length){
                                var image_keys = Utility.get_image_keys(block.responses[i].images);
                                keys = keys.concat(image_keys);
                            }
                            //Image
                            if(block.responses[i].image){
                                var image_keys = Utility.get_image_keys([block.responses[i].image.l], block.responses[i].image.m);
                                keys = keys.concat(image_keys);
                            }
                            //Attachments
                            if(block.responses[i].attachments && block.responses[i].attachments.length){
                                for(var j=0; j<block.responses[i].attachments.length; j++){
                                    var provider_key = Utility.get_provider_key(block.responses[i].attachments[j].provider);
                                    keys.push(provider_key);
                                }
                            }
                        }
                        callback();
                    } else {
                        callback();
                    }
                },
                function(callback){
                    //Delete images of mcq options
                    if(block.mcqs && block.mcqs.length){
                        for(var i=0; i<block.mcqs.length; i++){
                            if(block.mcqs[i].image){
                                var image_keys = Utility.get_image_keys([block.mcqs[i].image.l], block.mcqs[i].image.m);
                                keys = keys.concat(image_keys);
                            }
                        }
                        callback();
                    } else {
                        callback();
                    }
                },
                function(callback){
                    //Delete images of match the following options
                    if(block.options && block.options.length){
                        for(var i=0; i<block.options.length; i++){
                            if(block.options[i].image){
                                var image_keys = Utility.get_image_keys([block.options[i].image.l], block.options[i].image.m);
                                keys = keys.concat(image_keys);
                            }
                        }
                        callback();
                    } else {
                        callback();
                    }
                },
                function(callback){
                    //Delete images of items
                    if(block.items && block.items.length){
                        for(var i=0; i<block.items.length; i++){
                            if(block.items[i].image){
                                var image_keys = Utility.get_image_keys([block.items[i].image.l], block.items[i].image.m);
                                keys = keys.concat(image_keys);
                            }
                        }
                        callback();
                    } else {
                        callback();
                    }
                },
                function(callback){
                    //Delete all comments images
                    for(var i=0; i<block.comments.length; i++){
                        if(block.comments[i].images && block.comments[i].images.length){
                            var image_keys = Utility.get_image_keys(block.comments[i].images);
                            keys = keys.concat(image_keys);
                        }
                    }
                    callback();
                },
                function(callback){
                   //Update order of other blocks
                   var current_order = block.order;
                   Block.update({course: block.course, order: {$gt: current_order}}, { $inc : { order: -1 }}, {multi: true}, function(err, numAffected){
                       callback();
                   });
                },
                function(callback){
                    if(block.type == 'container'){
                        Block.update({container: block._id}, { $unset : { container: 1 }}, {multi: true}, function(err, numAffected){
                           callback();
                       });
                    } else {
                        callback();
                    }
                }
            ], function(err){
               if(!err){
                   //Delete block finally
                   block.remove(function(err){
                       if(!err){
                           res.sendStatus(200);
                           //Finally delete all keys
                           Utility.delete_keys(keys);
                       } else {
                           res.sendStatus(400);
                       }
                   });
               } else {
                   res.sendStatus(400);
               }
            });
        });
    });
};
/*---------------- BADGES FUNCTION -------------------------*/
//GET Request functions - Badges
//All badges of a course
var _getBadges = function(req, res){
    //Check access to course
    Course.findOne({
        _id: req.params._id,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}},
              {is_active: true, privacy: {$in: ['public', 'unlisted']}},
              {is_active: true, members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: {$in: ['teacher', 'active']}}}}]
    }, function(err, course){
        if(!course) return res.send([]);
        Badge.find({course: course._id}, function(err, badges){
            res.send(badges);
        });
    });
};
//Get a single badge
var _getBadgeById = function(req, res){

};
//POST Requests function
//Add badge to course
var _createBadge = function(req, res){
    if(!req.body.title) return res.status(400).send({error: "Invalid parameters. We are expecting a badge title."});
    if(!req.body.course) return res.status(400).send({error: "Invalid parameters. We are expecting a course id."});
    Course.findOne({
        _id: req.body.course,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }, function(err, course){
        if(!course) return res.status(400).send({error: "Unauthorized user. Cannot add badge to course."});
        //Get random color
        var badgeColor = randomColor({luminosity: 'dark'});
        //New badge
        var new_badge = new Badge({
            title: req.body.title,
            color: badgeColor,
            'image.m': req.body.image,
            'image.l': req.body.image,
            bound: req.body.bound,
            is_skill: req.body.is_skill,
            course: course._id,
            creator: req.user.id,
            updated_at: new Date(Date.now())
        });
        new_badge.save(function(err){
            if(!err) {
                //Update course count
                if(req.body.is_skill){
                    Course.update({ _id: course._id}, { $inc: { 'count.skills': 1 } }, function(err, numAffected){
                        res.send(new_badge);
                    });
                } else {
                    Course.update({ _id: course._id}, { $inc: { 'count.badges': 1 } }, function(err, numAffected){
                        res.send(new_badge);
                    });
                }
                //Update image
                if(req.body.image){
                    var key = shortid.generate();
                    var slug = key + '-' + getSlug(req.body.title);
                    var m_file_name = 'm-' + slug;
                    var image = req.body.image.replace(/^https:\/\//i, 'http://');
                    //Update image (medium size)
                    Utility.get_resized_image(m_file_name, image, 400, function(resized){
                        Utility.upload_file(resized, m_file_name, function(image_url){
                            Badge.update({ _id: new_badge._id }, { $set: { 'image.m': image_url }}).exec();
                        });
                    });
                }
            }
        });
    });
};
//DELETE Request function
//Delete comment
var _deleteBadge = function(req, res){
    Badge.findOne({_id: req.params._id}, function(err, badge){
        if(!badge) return res.sendStatus(404);
        //Update badge count
        async.series([
            function(callback){
                if(badge.is_skill){
                    Course.update({
                        _id: badge.course,
                        $or: [{creator: req.user.id},
                              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
                    }, { $inc: { 'count.skills': -1 } }, function(err, course){
                        callback();
                    });
                } else {
                    Course.update({
                        _id: badge.course,
                        $or: [{creator: req.user.id},
                              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
                    }, { $inc: { 'count.badges': -1 } }, function(err, course){
                        callback();
                    });
                }
            }
        ], function(err){
            //Get badge image keys
            if(badge.image) var keys = Utility.get_image_keys([badge.image.l], badge.image.m);
            //Remove badge
            badge.remove(function(err){
                res.sendStatus(200);
                //Delete badge images
                if(keys) Utility.delete_keys(keys);
            });
        });
    });
};
/*---------------- COMMENTS FUNCTION -------------------------*/
//GET Request functions - Comments
//All comments of a discussion
var _showComments = function(req, res){

};
//Get a single comment
var _getCommentById = function(req, res){

};
//POST Requests function
//Add a comment
var _addComment = function(req, res){
    if(!req.body.text || !req.body.block_id){
        return res.status(400).send({error: "Invalid parameters. We were expecting a text and a block id"});
    }
    //Find all courses user has access to
    Course.find({
        is_active: true,
        $or: [{privacy: {$in: ['public', 'unlisted']}},
              {creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: {$in: ['moderator', 'teacher', 'active']}}}}]
    }, function(err, courses){
        //Get course_ids
        var course_ids = [];
        if(courses && courses.length){
            for(var i=0; i<courses.length; i++){
                course_ids.push(courses[i]._id);
            }
        }
        //Get block
        Block.findOne({
            _id: req.body.block_id,
            has_discussion: true,
            course: {$in: course_ids}
        }).populate('creator', 'name email', 'User')
        .populate('comments.creator', 'name email', 'User').exec(function(err, block){
            if(!block) return res.status(400).send({error: "No such block exists."});
            if(req.body.reply_to){
                //Add indented reply
                var comments = block.comments;
                var ids = [], json = {};
                for(var i =0; i<comments.length; i++){
                    ids.push(comments[i]._id.toString());
                    //Get reply count
                    if(comments[i].reply_to){
                        if(json.hasOwnProperty(comments[i].reply_to)){
                            json[comments[i].reply_to]++;
                        } else {
                            json[comments[i].reply_to] = 1;
                        }
                    }
                }
                var new_reply = new Comment({
                    text: req.body.text,
                    images: req.body.images,
                    reply_to: req.body.reply_to,
                    creator: req.user.id,
                    updated_at: new Date(Date.now())
                });
                new_reply.summary = Utility.get_text_summary(new_reply.text);
                //Get comment order
                if(json.hasOwnProperty(req.body.reply_to)){
                    var comment_order = ids.indexOf(req.body.reply_to) + json[req.body.reply_to];
                } else {
                    var comment_order = ids.indexOf(req.body.reply_to);
                }
                if(comment_order > -1){
                    Block.update({_id: req.body.block_id}, {'$push': {'comments': {$each: [new_reply], $position: comment_order + 1 }}}, function(err, numAffected){
                        if(!err){
                            new_reply.populate({path: 'creator', select: 'name initials username dp'}, function(err, reply){
                                res.send(reply);
                            });
                        }
                    });
                }
            } else {
                //Remove previous recent
                Block.update({ _id: req.body.block_id, "comments.is_recent": true},
                      { $unset: { "comments.$.is_recent": 1 } }, function(err, numAffected) {
                    var new_comment = new Comment({
                        text: req.body.text,
                        images: req.body.images,
                        creator: req.user.id,
                        updated_at: new Date(Date.now()),
                        is_recent: true
                    });
                    new_comment.summary = Utility.get_text_summary(new_comment.text);
                    block.comments.push(new_comment);
                    block.save(function(err){
                        new_comment.populate({path: 'creator', select: 'name initials username dp job'},function(err, comment){
                            res.send(comment);
                        });
                    });
                });
            }
        });
    });
};
//PUT Requests function
//Edit comment
var _editComment = function(req, res){
    if(!req.body.text){
        return res.status(400).send({error: "Invalid parameters. We were expecting a comment text"});
    }
    //Get comment
    Block.findOne({
        'comments._id': req.params._id,
        'comments.creator': req.user.id
    }, function(err, block){
        if(!block) return res.status(400).send({error: "Unauthorized user. Cannot edit comment."});
        //Get comment
        var comments = block.comments;
        var comment;
        for(var i=0; i<comments.length; i++){
            if(comments[i]._id.toString() == req.params._id){
                comment = comments[i];
            }
        }
        //Get images
        var comment_images = [];
        if(req.body.images){
            if(comment.images){
                var comment_images = comment.images;
            }
            comment_images = _.union(comment_images, req.body.images);
        }
        //Get summary
        var summary = Utility.get_text_summary(req.body.text);
        //Update
        Block.update({
            'comments._id': req.params._id,
            'comments.creator': req.user.id
        }, { $set: { 'comments.$.text': req.body.text, 'comments.$.summary': summary, 'comments.$.images': comment_images} }, function(err, numAffected){
            if(!err) res.send({summary: summary, text: req.body.text});
        });
    });
};
//Like comment
var _likeComment = function(req, res){
    //Find all courses user has access to
    Course.find({
        is_active: true,
        $or: [{privacy: 'public'},
              {creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: {$in: ['moderator', 'teacher', 'active']}}}}]
    }, function(err, courses){
        //Get course_ids
        var course_ids = [];
        if(courses && courses.length){
            for(var i=0; i<courses.length; i++){
                course_ids.push(courses[i]._id);
            }
        }
        //Get block
        Block.update({
            'comments._id': req.params._id,
            has_discussion: true,
            course: {$in: course_ids}
        }, { $addToSet: { "comments.$.likes": req.user.id }}).exec(function(err, block){
            if(!err) {
                res.sendStatus(200);
            }
        });
    });
};
//Unlike comment
var _unlikeComment = function(req, res){
    Block.update({ 'comments._id': req.params._id},
    { $pull: { 'comments.$.likes': req.user.id } }, function(err, numAffected){
        if(!err) res.sendStatus(200);
    });
};
//DELETE Request function
//Delete comment
var _deleteComment = function(req, res){
    var block;
    async.series([
        function(callback){
            if(req.user.type == 'admin'){
                Block.findOne({
                    'comments._id': req.params._id
                }, function(err, b){
                    if(b) {
                        block = b;
                        callback();
                    }
                });
            } else {
                Block.findOne({
                    'comments._id': req.params._id,
                    'comments.creator': req.user.id
                }, function(err, b){
                    if(b){
                        block = b;
                        callback();
                    }
                });
            }
        }
    ], function(err){
        var only_comments = [], is_reply;
        for(var i=0; i< block.comments.length; i++){
            if(!block.comments[i].reply_to){
                only_comments.push(block.comments[i]);
            } else if(block.comments[i]._id.toString() == req.params._id){
                is_reply = true;
                break;
            }
        }
        //Get image keys
        var keys = [];
        for(var i=0; i<block.comments.length; i++){
            if((block.comments[i]._id.toString() == req.params._id) || (block.comments[i].reply_to && (block.comments[i].reply_to.toString() == req.params._id))){
                if(block.comments[i].images && block.comments[i].images.length){
                    var image_keys = Utility.get_image_keys(block.comments[i].images);
                    keys = keys.concat(image_keys);
                }
            }
        }
        //Find replies
        if(is_reply){
            //Delete reply
            Block.update({ 'comments._id': req.params._id}, {$pull: {comments: {_id: req.params._id}}}, function(err, numAffected){
                if(!err) res.sendStatus(200);
                //Finally delete all keys
                Utility.delete_keys(keys);
            });
        } else {
            //Delete comment and its replies
            Block.update({'comments.reply_to': req.params._id}, {$pull: {comments: {reply_to: req.params._id}}}, function(err, numAffected){
                Block.update({ 'comments._id': req.params._id}, {$pull: {comments: {_id: req.params._id}}}, function(err, numAffected){
                    if(!err) {
                        //Make previous comment active
                        if(only_comments.length > 1 && only_comments[only_comments.length -1]._id.toString() == req.params._id){
                            var prev_id = only_comments[only_comments.length -2]._id;
                            Block.update({ 'comments._id': prev_id}, { $set: { 'comments.$.is_recent': true } }, function(err, numAffected) {
                                    res.sendStatus(200);
                                    //Finally delete all keys
                                    Utility.delete_keys(keys);
                                });
                        } else {
                            res.sendStatus(200);
                            //Finally delete all keys
                            Utility.delete_keys(keys);
                        }
                    }
                });
            });
        }
    });
};
/* ----------------- USER FUNCTION ------------------ */
//GET Request functions - User
//Get details of current user
var _getCurrentUser = function(req, res){
    User.findOne({_id: req.user.id}).select('-prev_password -loginAttempts -lockUntil -requestToken -resetPasswordToken -resetPasswordExpires')
    .exec(function(err, user){
        user.password = '';
        res.send(user);
    });
};
//POST Request functions - User
//Update current user
var _updateCurrentUser = function(req, res){
    User.findOne({_id: req.user.id}).select('name initials about email dp password').exec(function(err, user){
        var auser = user;
        if(req.body.oldpwd && req.body.newpwd && user.validPassword(req.body.oldpwd) && req.body.name){
            user.name = req.body.name;
            user.initials = req.body.name.split(' ').map(function (s) { return s.charAt(0); }).join('').toUpperCase();
            if(req.body.about != null){
                var linkifiedText = linkifyHtml(req.body.about, {
                    target: '_blank'
                });
                user.about = linkifiedText.replace(/\n\r?/g, '<br />');
            }
            user.job.title = req.body.job.title;
            user.job.org = req.body.job.org;
            user.country = req.body.country;
            user.city = req.body.city;
            user.phone = req.body.phone;
            if(req.body.sex){
                user.sex = req.body.sex;
            }
            user.prev_password = user.password;
            user.password = user.generateHash(req.body.newpwd);
            user.save(function(err){
                user.password = null;
                user.prev_password = null;
                res.send(user);
            });
        } else if(req.body.name){
            user.name = req.body.name;
            user.initials = req.body.name.split(' ').map(function (s) { return s.charAt(0); }).join('').toUpperCase();
            if(req.body.about != null){
                var linkifiedText = linkifyHtml(req.body.about, {
                    target: '_blank'
                });
                user.about = linkifiedText.replace(/\n\r?/g, '<br />');
            }
            user.job.title = req.body.job.title;
            user.job.org = req.body.job.org;
            user.country = req.body.country;
            user.city = req.body.city;
            user.phone = req.body.phone;
            if(req.body.sex){
                user.sex = req.body.sex;
            }
            user.save(function(err){
                user.password = null;
                res.send(user);
            });
        } else if(req.body.dp) {
            user.dp.m = req.body.dp;
            user.dp.s = req.body.dp;
            user.save(function(err){
                if(!err) {
                    user.password = null;
                    res.send(user);
                }
                //Resize image
                var key = uuid.v4();
                var file_name = key + '-' + getSlug(user.name);
                var dp = req.body.dp.replace(/^https:\/\//i, 'http://');
                Utility.get_resized_image(file_name, dp, 100, function(resized){
                    Utility.upload_file(resized, file_name, function(image_url){
                        User.update({ _id: req.user.id }, { $set: { 'dp.s': image_url }}).exec();
                    });
                });
            });
        }
    });
};
//Get all active users for admin
var _getAllActiveUsers = function(req, res){
    //Check if admin
    if(req.user.type != 'admin'){
        return res.status(400).send({error: "Unauthorized user. Cannot view"});
    }
    User.find({type: {$ne: 'invited'}}).select('_id email initials username name dp city country sex about job').sort({accountCreated: -1}).exec(function(err, users){
        res.send(users);
    });
};
//Get all inactive users for admin
var _getAllInactiveUsers = function(req, res){
    //Check if admin
    if(req.user.type != 'admin'){
        return res.status(400).send({error: "Unauthorized user. Cannot view"});
    }
    User.find({type: 'invited'}).select('_id email initials username name dp city country sex about job').sort({accountCreated: -1}).exec(function(err, users){
        res.send(users);
    });
};
//Get user details
var _getPublicUser = function(req, res){
    //Match if object id or not
    if(req.params._id.match(/^[0-9a-fA-F]{24}$/)){
        var query = {
            _id: req.params._id
        };
    } else {
        var query = {
            username: req.params._id
        };
    }
    //Find
    User.findOne(query).select('name accountCreated initials username about dp job city country').exec(function(err, user){
        if(!user) return res.status(400).send({error: "No such user exists"});
        res.send(user);
    });
};
//Activate user
var _activateUser = function(req, res){
    //Check if admin
    if(req.user.type != 'admin'){
        return res.status(400).send({error: "Unauthorized user. Cannot activate user."});
    }
    var user_type = req.body.type || 'normal';
    User.update({ _id: req.params._id}, { $set : { type: user_type } }, function(err, numAffected){
        res.sendStatus(200);
    });
};
//Deactivate user
var _deactivateUser = function(req, res){
    //Check if admin
    if(req.user.type != 'admin'){
        return res.status(400).send({error: "Unauthorized user. Cannot deactivate user."});
    }
    User.update({ _id: req.params._id}, { $set : { type: 'invited' } }, function(err, numAffected){
        res.sendStatus(200);
    });
};
//Update unique id
var _updateUniqueId = function(req, res){
    //Get unique name
    const {uniqueNamesGenerator, adjectives, animals, colors, names} = require('unique-names-generator');
    const randomName = uniqueNamesGenerator({
        dictionaries: [adjectives, animals, colors, names],
        length: 4,
        separator: "-"
    });
    User.update({_id: req.user.id}, {$set : {email: randomName.toLowerCase()}}, function(err, numAffected){
        res.send({id: randomName.toLowerCase()});
    });
};
/*---------------- SEARCH FUNCTION -------------------------*/
//GET Request functions
//Search users
var _searchUsers = function(req, res){
    if(!req.query.text) return res.status(400).send({error: "Invalid parameters. We are expecting a search text."});
    var page = req.query.page;
    //Excluded
    if(req.query.excluded){
        var excluded = JSON.parse(req.query.excluded);
    }
    //Find
    User.find({
        _id: {$nin: excluded},
        $or: [{ 'name': new RegExp('' + req.query.text + '', "i")},
              { 'email': new RegExp('' + req.query.text + '', "i") }] })
    .select('name initials username dp job')
    .skip((page - 1)*PAGE_SIZE).limit(PAGE_SIZE)
    .exec(function(err, users){
        res.send(users);
    });
};
//Search gifs
var _searchGifs = function(req, res){
    if(!req.query.search) return res.status(400).send({error: "Invalid parameters. We are expecting a search text"});
    Utility.get_gifs_results(req.query.search, function(data){
        res.status(200).send(data);
    });
};
//Search badges
var _searchBadges = function(req, res){
    if(!req.query.text) return res.status(400).send({error: "Invalid parameters. We are expecting a search text."});
    if(!req.query.course) return res.status(400).send({error: "Invalid parameters. We are expecting a course id."});
    Course.findOne({
        _id: req.query.course,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }, function(err, course){
        if(!course) return res.send([]);
        var page = req.query.page;
        //Excluded
        if(req.query.excluded){
            var excluded = JSON.parse(req.query.excluded);
        }
        //Find badges
        Badge.find({
            course: course._id,
            is_skill: false,
            _id: {$nin: excluded},
            title: new RegExp('' + req.query.text + '', "i")
        }).select('title color')
        .skip((page - 1)*PAGE_SIZE).limit(PAGE_SIZE)
        .exec(function(err, badges){
            res.send(badges);
        });
    });
};
//Search skills
var _searchSkills = function(req, res){
    if(!req.query.text) return res.status(400).send({error: "Invalid parameters. We are expecting a search text."});
    if(!req.query.course) return res.status(400).send({error: "Invalid parameters. We are expecting a course id."});
    Course.findOne({
        _id: req.query.course,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }, function(err, course){
        if(!course) return res.send([]);
        var page = req.query.page;
        //Excluded
        if(req.query.excluded){
            var excluded = JSON.parse(req.query.excluded);
        }
        //Find badges
        Badge.find({
            course: course._id,
            is_skill: true,
            _id: {$nin: excluded},
            title: new RegExp('' + req.query.text + '', "i")
        }).select('title color')
        .skip((page - 1)*PAGE_SIZE).limit(PAGE_SIZE)
        .exec(function(err, skills){
            res.send(skills);
        });
    });
};
/*---------------- INSIGHT FUNCTION -------------------------*/
//GET Request functions
//Get basic insight
var _getCourseBasicInsight = function(req, res){
    Course.findOne({
        _id: req.params._id,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }, function(err, course){
        if(!course) return res.status(400).send({error: "Unauthorized user. Cannot get insight."});
        var all_blocks_count = 0, response_blocks_count = 0, discussion_blocks_count = 0, comments_count = 0;
        async.parallel([
            function(callback){
                Block.count({course: course._id}, function(err, all_blocks){
                    all_blocks_count = all_blocks;
                    callback();
                });
            },
            function(callback){
                Block.count({course: course._id, type: {$in: ['mcq', 'fill', 'match', 'response']}}, function(err, response_blocks){
                    response_blocks_count = response_blocks;
                    callback();
                });
            },
            function(callback){
                Block.count({course: course._id, has_discussion: true}, function(err, discussion_blocks){
                    discussion_blocks_count = discussion_blocks;
                    callback();
                });
            },
            function(callback){
                Block.find({course: course._id, has_discussion: true}, function(err, discussion_blocks){
                    for(var i=0; i<discussion_blocks.length; i++){
                        comments_count += discussion_blocks[i].comments.length;
                    }
                    callback();
                });
            }
        ], function(err){
            if(!err){
                res.send({
                    all_blocks_count: all_blocks_count,
                    response_blocks_count: response_blocks_count,
                    discussion_blocks_count: discussion_blocks_count,
                    comments_count: comments_count
                });
            }
        });
    });
};
//Get all users of course
var _getCourseUsersInsight = function(req, res){
    Course.findOne({
        _id: req.params._id,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }, function(err, course){
        if(!course) return res.status(400).send({error: "Unauthorized user. Cannot get insight."});
        Block.find({course: course._id, type: {$in: ['mcq', 'fill', 'match', 'response']}}, function(err, response_blocks){
            var user_ids = [], mcq_user_ids = [], fill_user_ids = [], match_user_ids = [], response_user_ids = [], comment_user_ids = [];
            async.each(response_blocks, function(block, callback){
                if(block.type == 'mcq'){
                    //MCQ
                    var options = block.mcqs;
                    if(options.length){
                        for(var i=0; i<options.length; i++){
                            var voters = options[i].voters;
                            if(voters.length){
                                for(var j=0; j<voters.length; j++){
                                    var response_user = voters[j].toString();
                                    if(user_ids.indexOf(response_user) > - 1) continue;
                                    else {
                                        user_ids.push(response_user);
                                        mcq_user_ids.push(response_user);
                                    }
                                }
                            } else {
                                continue;
                            }
                        }
                        callback();
                    } else {
                        callback();
                    }
                } else if(block.type == 'fill'){
                    //Fill in the blanks
                    var fills = block.fills;
                    if(fills.length){
                        for(var i=0; i<fills.length; i++){
                            var responses = fills[i].responses;
                            if(responses.length){
                                for(var j=0; j<responses.length; j++){
                                    var response_user = responses[j].creator.toString();
                                    if(user_ids.indexOf(response_user) > - 1) continue;
                                    else {
                                        user_ids.push(response_user);
                                        fill_user_ids.push(response_user);
                                    }
                                }
                            } else {
                                continue;
                            }
                        }
                        callback();
                    } else {
                        callback();
                    }
                } else if(block.type == 'match'){
                    //Match the following
                    var options = block.options;
                    if(options.length){
                        for(var i=0; i<options.length; i++){
                            var responses = options[i].matchers;
                            if(responses.length){
                                for(var j=0; j<responses.length; j++){
                                    var response_user = responses[j].creator.toString();
                                    if(user_ids.indexOf(response_user) > - 1) continue;
                                    else {
                                        user_ids.push(response_user);
                                        match_user_ids.push(response_user);
                                    }
                                }
                            } else {
                                continue;
                            }
                        }
                        callback();
                    } else {
                        callback();
                    }
                } else {
                    //Response
                    var responses = block.responses;
                    if(responses.length){
                        for(var i=0; i<responses.length; i++){
                            var response_user = responses[i].creator.toString();
                            if(user_ids.indexOf(response_user) > - 1) continue;
                            else {
                                user_ids.push(response_user);
                                response_user_ids.push(response_user);
                            }
                        }
                        callback();
                    } else {
                        callback();
                    }
                }
            }, function(err){
                if(user_ids.length){
                    //Find unique comments
                    Block.find({course: course._id, has_discussion: true}, function(err, discussion_blocks){
                        if(discussion_blocks.length){
                            for(var i=0; i<discussion_blocks.length; i++){
                                var comments = discussion_blocks[i].comments;
                                if(comments.length){
                                    for(var j=0; j<comments.length; j++){
                                        var response_user = comments[j].creator.toString();
                                        if(user_ids.indexOf(response_user) > - 1) continue;
                                        else user_ids.push(response_user);
                                    }
                                } else {
                                    continue;
                                }
                            }
                            //Find users
                            User.find({_id: {$in: user_ids}}).select('_id email initials username name dp city country sex about job').exec(function(err, users){
                                res.send({
                                    mcq_user_ids: mcq_user_ids.length,
                                    fill_user_ids: fill_user_ids.length,
                                    match_user_ids: match_user_ids.length,
                                    response_user_ids: response_user_ids.length,
                                    users: users
                                });
                            });
                        } else {
                            User.find({_id: {$in: user_ids}}).select('_id email initials username name dp city country sex about job').exec(function(err, users){
                                res.send({
                                    mcq_user_ids: mcq_user_ids.length,
                                    fill_user_ids: fill_user_ids.length,
                                    match_user_ids: match_user_ids.length,
                                    response_user_ids: response_user_ids.length,
                                    users: users
                                });
                            });
                        }
                    });
                } else {
                    res.send([]);
                }
            });
        });
    });
};
//Get one user responses
var _getCourseUserInsight = function(req, res){
    if(!req.query.user) return res.status(400).send({error: "Invalid parameters. We are expecting a user id."});
    Course.findOne({
        _id: req.params._id,
        $or: [{creator: req.user.id},
              {members: { $elemMatch: { user: mongoose.Types.ObjectId(req.user.id), permit_val: 'moderator'}}}]
    }, function(err, course){
        if(!course) return res.status(400).send({error: "Unauthorized user. Cannot get insight."});
        User.findOne({_id: req.query.user}).select('_id email initials username name dp city country sex about job').exec(function(err, user){
            if(!user) return res.status(400).send({error: "Invalid parameters. We are expecting a user id."});
            var all_blocks = [];
            Block.find({
                course: course._id,
                $or: [{type: {$in: ['mcq', 'fill', 'match', 'response']}}, {has_discussion: true}]
            }).select({ order: 1, slug: 1, type: 1, course: 1, is_active: 1, is_hidden: 1, title: 1, summary: 1, text: 1, button: 1, divider: 1, image: 1, bound: 1, file: 1, provider: 1, embed: 1, gif: 1, mcqs: 1, is_multiple: 1, fills: 1, options: 1, response_type:1, responses: { $elemMatch: { creator: req.query.user }}, items: 1, theme: 1, art: 1, size: 1, feedbacks: 1, alt_text: 1, ref_url: 1, extra: 1, has_discussion: 1, comments: { $elemMatch: { creator: req.query.user }}, creator: 1, created_at: 1, updated_at: 1}).sort({order: 1}).exec(function(err, blocks){
                var all_blocks = [];
                async.each(blocks, function(block, callback){
                    if(block.type == 'mcq'){
                        //MCQ
                        var can_add_block = false;
                        var user_options = [];
                        var options = block.mcqs;
                        if(options.length){
                            for(var i=0; i<options.length; i++){
                                var voters = options[i].voters;
                                if(voters.indexOf(user._id) > -1){
                                    options[i].voters = [];
                                    user_options.push(options[i]);
                                    can_add_block = true;
                                } else {
                                    continue;
                                }
                            }
                            block.mcqs = user_options;
                            if(can_add_block) all_blocks.push(block);
                            callback();
                        } else {
                            callback();
                        }
                    } else if(block.type == 'fill'){
                        //Fill in the blanks
                        var can_add_block = false;
                        var fills = block.fills;
                        if(fills.length){
                            for(var i=0; i<fills.length; i++){
                                var responses = fills[i].responses;
                                if(responses.length){
                                    var user_responses = [];
                                    for(var j=0; j<responses.length; j++){
                                        if(user._id == responses[j].creator.toString()){
                                            user_responses.push(responses[j]);
                                            can_add_block = true;
                                        }
                                    }
                                    block.fills[i].responses = user_responses;
                                } else {
                                    continue;
                                }
                            }
                            if(can_add_block) all_blocks.push(block);
                            callback();
                        } else {
                            callback();
                        }
                    } else if(block.type == 'match'){
                        //Match the following
                        var can_add_block = false;
                        var options = block.options;
                        if(options.length){
                            for(var i=0; i<options.length; i++){
                                var responses = options[i].matchers;
                                if(responses.length){
                                    var user_responses = [];
                                    for(var j=0; j<responses.length; j++){
                                        if(user._id == responses[j].creator.toString()){
                                            user_responses.push(responses[j]);
                                            can_add_block = true;
                                        }
                                    }
                                    block.options[i].matchers = user_responses;
                                } else {
                                    continue;
                                }
                            }
                            if(can_add_block) all_blocks.push(block);
                            callback();
                        } else {
                            callback();
                        }
                    } else if(block.type == 'response'){
                        //Response
                        var can_add_block = false;
                        var user_responses = [];
                        var responses = block.responses;
                        if(responses.length){
                            for(var i=0; i<responses.length; i++){
                                if(user._id == responses[i].creator.toString()){
                                    user_responses.push(responses[i]);
                                    can_add_block = true;
                                } else continue;
                            }
                            block.responses = user_responses;
                            if(can_add_block) all_blocks.push(block);
                            callback();
                        } else {
                            callback();
                        }
                    } else {
                        callback();
                    }
                }, function(err){
                    res.send({
                        user: user,
                        blocks: all_blocks
                    });
                });
            });
        });
    });
};
/* ------------------- LINK PREVIEW FUNCTION ------------- */
var _getLinkPreview = function(req, res){
    if(!req.query.url && !validator.isURL(req.query.url))
        return res.status(400).send({error: "Invalid parameters. We are expecting a valid url"});
    Utility.get_link_metadata(req.query.url, function(data){
        res.status(200).send(data);
    });
};
/* ------------------- UNIQUE NAME FUNCTION ------------- */
var _getUniqueName = function(req, res){
    const {uniqueNamesGenerator, adjectives, animals, colors, names} = require('unique-names-generator');
    const randomName = uniqueNamesGenerator({
        dictionaries: [adjectives, animals, colors, names],
        length: 4,
        separator: "-"
    });
    res.status(200).send({name: randomName.toLowerCase()});
};
/* ------------------- ANALYSIS FUNCTION ------------- */
//Get dominant language
var _getDominantLanguage = function(req, res){
    if(!req.body.text){
        return res.status(400).send({error: "Invalid parameters. We are expecting a text array."});
    }
    //Analyze
    Analysis.get_dominant_language(req.body.text, function(data){
        res.send({data: data});
    });
};
//Get named entities
var _getEntityRecognition = function(req, res){
    if(!req.body.text){
        return res.status(400).send({error: "Invalid parameters. We are expecting a text array."});
    }
    //Analyze
    Analysis.get_entity_recognition(req.body.text, function(data){
        res.send({data: data});
    });
};
//Get keyphrases
var _getKeyPhrases = function(req, res){
    if(!req.body.text){
        return res.status(400).send({error: "Invalid parameters. We are expecting a text array."});
    }
    //Analyze
    Analysis.get_key_phrases(req.body.text, function(data){
        res.send({data: data});
    });
};
//Get sentiments of a text
var _getSentimentAnalysis = function(req, res){
    if(!req.body.text){
        return res.status(400).send({error: "Invalid parameters. We are expecting a text array."});
    }
    //Analyze
    Analysis.get_sentiment_analysis(req.body.text, function(data){
        res.send({data: data});
    });
};
//Get parts of speech
var _getSyntaxAnalysis = function(req, res){
    if(!req.body.text){
        return res.status(400).send({error: "Invalid parameters. We are expecting a text array."});
    }
    //Analyze
    Analysis.get_syntax_analysis(req.body.text, function(data){
        res.send({data: data});
    });
};
//Get translated text
var _getTranslatedText = function(req, res){
    if(!req.body.text){
        return res.status(400).send({error: "Invalid parameters. We are expecting a text value."});
    }
    //Analyze
    Analysis.get_translated_text(req.body.text, function(data){
        res.send({data: data});
    });
};
//Get tone analysis
var _getToneAnalysis = function(req, res){
    if(!req.body.text){
        return res.status(400).send({error: "Invalid parameters. We are expecting a text array."});
    }
    //Analyze
    Analysis.get_tone_analysis(req.body.text, function(data){
        res.send({data: data});
    });
};
/* ------------------- UPLOAD TO S3 ------------- */
var _uploadS3 = function(req, res){
    var mime_type = mime.lookup(req.query.title);
    var expire = moment().utc().add(1, 'hour').toJSON("YYYY-MM-DDTHH:mm:ss Z");
    var policy = JSON.stringify({
      "expiration": expire,
        "conditions": [
          {"bucket": process.env.AWS_BUCKET},
          ["starts-with", "$key", process.env.BUCKET_DIR],
          {"acl": "public-read"},
          {"success-action-status": "201"},
          ["starts-with", "$Content-Type", mime_type],
          ["content-length-range", 0, process.env.MAX_FILE_SIZE]
        ]
    });
    var base64policy = new Buffer(policy).toString('base64');
    var signature = crypto.createHmac('sha1', process.env.AWS_SECRET).update(base64policy).digest('base64');
    var file_key = uuid.v4();
    res.json({
        policy: base64policy,
        signature: signature,
        key: process.env.BUCKET_DIR + file_key + "_" + req.query.title,
        success_action_redirect: "/",
        contentType: mime_type
  });
};
//Route middleware to check if user is loggedIn
function isLoggedIn(req, res, next){
    //passport function to check session and cookie
    if(req.isAuthenticated())
        return next();
    //redirect to login page if not loggedin
    res.redirect('/login');
};
